import React, { useState } from "react";
import axiosInstance from "../components/axiosApi"

function Signup() {
    const [dataSignUp,setDataSignUp] = useState(
        {
            email:"",
            password:"",
            name:"",
            is_publisher:false,
            errors: {
                email:"",
                password:"",
                name:"",
                is_publisher:"", 
            }
        }
    )

    const handleDataSignUp = (e) => {
        setDataSignUp({...dataSignUp, [e.target.name]:e.target.value})
    }

    const handleDataSignUpCheck = (e) => {
        setDataSignUp({...dataSignUp, [e.target.name]:e.target.checked})
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        await axiosInstance.post('/user/create/', {
            email: dataSignUp.email,
            password: dataSignUp.password,
            name: dataSignUp.name,
            is_publisher: dataSignUp.is_publisher
        }).catch (error => {
            console.log(error.stack);
            setDataSignUp({...dataSignUp, errors:error.response.data})
        })
        setDataSignUp({
            email: "",
            password: "",
            name:"",
            is_publisher:false,
            errors:""
        })
    }

    return (
        <div>
            <h2>Signup page</h2>
            <form onSubmit={handleSubmit}>
                <div>
                    <label htmlFor="email">Email:</label>
                    <input name="email" id="email" type="email" onChange={handleDataSignUp} value={dataSignUp.email}/>
                    { dataSignUp.errors.email ? dataSignUp.errors.email : null}
                </div>
                <div>
                    <label htmlFor="password">Password:</label>
                    <input name="password" id="password" type="password" onChange={handleDataSignUp} value={dataSignUp.password}/>
                    { dataSignUp.errors.password ? dataSignUp.errors.password : null}
                </div>
                <div>
                    <label htmlFor="name">Name</label>
                    <input type="text" onChange={handleDataSignUp} value={dataSignUp.name} id="name" name="name"/>
                    { dataSignUp.errors.name ? dataSignUp.errors.name : null}
                </div>
                <div>
                    <input type="checkbox" onChange={handleDataSignUp} checked={dataSignUp.is_publisher} id="is_publisher" name="is_publisher"/>
                    <label htmlFor="is_publisher">Publisher?</label>
                    { dataSignUp.errors.is_publisher ? dataSignUp.errors.is_publisher : null}
                </div>
                <button type="submit">Submit</button>
            </form>
        </div>
    )
}
export default Signup;