import React, { useState } from 'react';
import Carousel from 'react-elastic-carousel';

import "./homepage.css";

import telor3 from "../../image/telor3.png";
import elips13 from "../../image/Ellipse13.png";
import benang1 from "../../image/benang1.png";
import telor1_kiri from "../../image/telor1_kiri.png";
import telor1_kanan from "../../image/telor1_kanan.png";
import telordalem from "../../image/telor-dalem.png";
import telorluar from "../../image/telor-luar.png";
import pink_kiri from "../../image/pink_kiri.png";
import pink_kanan from "../../image/pink_kanan.png";
import biru_kiri from "../../image/biru_kiri.png";
import biru_kanan from "../../image/biru_kanan.png";
import ijo_kiri from "../../image/ijo_kiri.png";
import ijo_kanan from "../../image/ijo_kanan.png";
import telorpink from "../../image/telorpink.png";
import telorijo from "../../image/telorijo.png";
import telorkuning from "../../image/telorkuning.png";
import foto1 from "../../image/image_1.png";
import foto2 from "../../image/image_2.png";
import foto3 from "../../image/image_3.png";
import foto4 from "../../image/image_4.png";

import Navbar from "../navbar/navbar";

import piala from "../../image/piala.png";
import kalender from "../../image/kalender.png";
import buku from "../../image/buku.png";

import sherly from "../../image/Sherly.png";
import Kartini from "../../image/Kartini.png";

import logo_tentang from "../../image/logotentang.png";

import zakatsukses from "../../image/sponsor-zakatsukses.png";
import grupsponsor from "../../image/sponsor-grup.png";

import linkaja1 from "../../image/linkaja1.jpg";
import linkaja2 from "../../image/linkaja2.jpg";
import linkaja3 from "../../image/linkaja3.jpg";

export default function Homepage() {
    const [modalActive, setModalActive] = useState(false);

    const contents = [
        linkaja1,
        linkaja2,
        linkaja3
    ]
    
    let modal;

    console.warn(modalActive);

    function handleSponsor(event) {
        setModalActive(!modalActive);
        console.log(modalActive);
    }

    if(modalActive) {
        console.log("TEST")
        modal = (
        <div className='modal'>
          <div className='modal-inner'>
            <div className='modal-header'></div>
            <div className='modal-introduction'>
              <Carousel>
                    {contents.map(content => <img src={content} alt="carousel item"></img>)}
              </Carousel>
            </div>
            <button
              className='btn btn-danger btn-block btn-close'
              onClick={handleSponsor}
            >
                Tutup
            </button>
          </div>
        </div>
        );
    }



    return (
        <div className="tampungan">
            <Navbar 
                beranda = "active"
            />

            <div className="background-particle">
                <div className="half-elips"><img src={elips13} alt="elips"/></div>
                <div className="benang-atas"><img src={benang1} alt="benang"/></div>
                <div className="telor3"><img src={telor3} alt="telor3"/></div>
                <div className="telor1_kiri"><img src={telor1_kiri} alt="telor"/></div>
                <div className="telor1_kanan"><img src={telor1_kanan} alt="telor"/></div>
                <div className="telor-dalem"><img src={telordalem} alt="telor"/></div>
                <div className="telor-luar"><img src={telorluar} alt="telor"/></div>
                <div className="pink-kiri"><img src={pink_kiri} alt="elipskiri"/></div>
                <div className="ijo-kiri"><img src={ijo_kiri} alt="elipskiri"/></div>
                <div className="biru-kiri"><img src={biru_kiri} alt="elipskiri"/></div>
                <div className="pink-kanan"><img src={pink_kanan} alt="elipskanan"/></div>
                <div className="ijo-kanan"><img src={ijo_kanan} alt="elipskanan"/></div>
                <div className="biru-kanan"><img src={biru_kanan} alt="elipskanan"/></div>
                <div className="telor-pink"><img src={telorpink} alt="telor"/></div>
                <div className="telor-ijo"><img src={telorijo} alt="telor"/></div>
                <div className="telor-kuning"><img src={telorkuning} alt="telor"/></div>
            </div>

            <div className="container cont">
                <div className="present-home col-12">
                    <div className="left-present col-8">
                        <h4>UI IBF 2020 Present:</h4>
                        <h1>From The Word  to The Whole World Strive for Ummah’s Resiliency through Literacy</h1>
                        <h6>Mari wujudkan kesatuan ummat yang tangguh dan kuat bersama UI Islamic Book Fair!</h6>
                        <a 
                            // onLoad={window.alert("Hari ini Grand Launching!\nSaksikan sekarang!")}
                            href="https://youtu.be/Ilt67MI0za4"
                            className="btn btn-outline-info"
                        >
                            Saksikan Grand Launching UI IBF disini!
                        </a>
                        <div onClick={handleSponsor} className="btn btn-info btn-sponsor">Psstt, ada Link Aja disini</div>

                        {modal}
                    </div>

                    <div className="right-present col-4">
                        <div className="foto-atas">
                            <div className="col-6 atas-a">
                                <img src={foto1} alt="foto"/>
                            </div>

                            <div className="col-6 atas-b">
                                <img src={foto2} alt="foto"/>
                            </div>
                        </div>

                        <div className="foto-bawah">
                            <div className="col-6 bawah-a">
                                <img src={foto3} alt="foto"/>
                            </div>
                            <div className="col-6 bawah-b">
                                <img src={foto4} alt="foto"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="col4"></div>
                <div className="col-6 eksplor">
                    <h1>Eksplor niatmu bersama UIIBF lewat Kompetisi, Acara dan Bookfair!</h1>
                </div>

                <div className="ada-apa">
                    <div className="col-4">
                        <div className="tile">
                            <img src={piala} alt="piala"/>

                            <h2>Kompetisi</h2>

                            <p>Berlomba-lombalah dalam berbuat kebaikan dan juga dalam mengembangkan bakat.</p>

                            <a href="/kompetisi">See More ></a>
                        </div>
                    </div>
                    <div className="col-4">
                        <div className="tile">
                            <img src={kalender} alt="kalender"/>

                            <h2>Acara</h2>

                            <p>Ikuti rangkaian acara berupa seminar, workshop dan bedah buku secara daring di UIIBF!</p>
                            
                            <a href="/acara">See More ></a>
                        </div>
                    </div>
                    <div className="col-4">
                        <div className="tile">
                            <img src={buku} alt="buku"/>

                            <h2>Bookfair</h2>

                            <p>Perbanyak pahala dan ilmu dengan mengoleksi buku sebagai sumber pengetahuan.</p>
                            
                            <a href="/">See More ></a>
                        </div>
                    </div>
                </div>

                <div className="they-said">
                    <h1>Apa Kata Mereka?</h1>

                    <div className="kata-kata">
                        <div className="kata-orang">
                            <div className="foto-orang">
                                <img src={sherly} alt="orang"/>
                            </div>

                            <div className="perkataan text-justify">
                                <p>
                                    “ Sherly sangat terkesan dengan acara UIIBF 
                                    ini. Merasakan langsung atmosfer dimana seluruh  
                                    anak muda berkumpul untuk membicarakan suatu 
                                    permasalahan yang semoga keluar dari sini bukan 
                                    hanya sekadar mengambil ilmu baru tetapi juga 
                                    mengambil langkah eksekusi bagi dirinya sendiri..."“
                                </p>
                                <h4>Sherly Annavita Rahmi, S.Sos, M.SIph</h4>
                                <h6>Milenial Influencer</h6>
                            </div>
                        </div>
                        
                        <div className="kata-orang">
                            <div className="foto-orang">
                                <img src={Kartini} alt="orang"/>
                            </div>

                            <div className="perkataan text-justify">
                                <p>
                                    “ Acara Bedah bukunya seru banget.  
                                    Di Acara UIIBF juga  banyak ketemu insight 
                                    untuk membuat karya baru.. ”
                                </p>
                                <h4>Kartini F Astuti</h4>
                                <h6>Penulis, Alumni DKV ITB</h6>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="tentang">
                    <div className="logo-tentang">
                        <img src={logo_tentang} alt="logo"/>
                    </div>

                    <div className="desc-tentang">
                        <h1>Tentang Kami</h1>

                        <p>
                        UI Islamic Bookfair (UI IBF) ini merupakan agenda syiar 
                        literasi terbesar yang diselenggarakan oleh LDK Salam UI 
                        sejak tahun 2015.  Lebih dari 24.000 pengunjung dari 
                        berbagai daerah di Indonesia turut meramaikan UI IBF 
                        yang dilaksanakan pada tahun sebelumnya. 
                        </p>
                        <p style={{marginTop: 0}}>
                        Nah, Tahun ini, kami hadir secara virtual! Kompetisi , 
                        Bookfair, Seminar dan bedah buku bisa teman-teman hadiri 
                        secara daring, loh! Jadi tunggu apa lagi? Langsung saja 
                        jelajahi website kami dan rasakan keseruannya!
                        </p>

                        <div className="tentang-medsos">
                            <a href="https://instagram.com/uiibf" target="_blank" rel="noopener noreferrer">
                                <i class="fab fa-instagram follow-instagram"></i>
                            </a>
                            <a href="https://www.youtube.com/channel/UCUutW1j5c_ujoI9DH7FTYsQ" target="_blank" rel="noopener noreferrer">
                                <i class="fab fa-youtube follow-youtube"></i>
                            </a>
                            <a href="https://twitter.com/uiibf" target="_blank" rel="noopener noreferrer">
                                <i class="fab fa-twitter follow-twitter"></i>
                            </a>
                        </div>
                    </div>
                </div>

                <div className="row justify-content-center w-100 mt-5">
                    <div className="col-12">
                        <h4 className="text-center">Sponsor Kami</h4>
                        <div id="sponsor-margin" className="d-flex flex-wrap justify-content-center">
                            <div className="">
                                <img className="img-fluid" alt="Sponsor 1" src={zakatsukses}></img>
                            </div>
                            <div className="">
                                <img className="img-fluid" alt="Grup Sponsor" src={grupsponsor}></img>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    )
}
