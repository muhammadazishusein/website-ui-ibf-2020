import React, { useState } from 'react';
import { Link } from "react-router-dom";
// import logo from "../../image/logo.png";
// import logoelips from "../../image/logoelips.png";
import logoibf from "../../image/logouiibf6.png";
// import logo_ig from "../../image/logo_ig.png";
// import logo_yt from "../../image/logo_yt.png";
// import logo_tw from "../../image/logo_tw.png";
import "./footer.css";

export default function Footer () {
    const [click] = useState(false)

    return(
        <footer>
            <div className="container-foot">
                <div className="footer-bottom-left">
                    <div className="logo-footer">
                        <img src={logoibf} alt="Logo UI UBF 6" width="100%"/>
                    </div>

                    <div className="desc-footer">
                        <h2>UI Islamic Bookfair 2020</h2>

                        <p className="jargon-foot">From The Word  to The Whole World Strive for Ummah’s Resiliency through Literacy</p>

                        <h5 className="copyright-foot"> &copy; UI IBF 2020</h5>
                    </div>
                </div>


                <div className="footer-bottom-right">
                    <div className="follow-us">
                        <h4>Ikuti Kami!</h4>

                        <div className="bottom-medsos">
                            <a href="https://instagram.com/uiibf" target="_blank" rel="noopener noreferrer">
                            <i class="fab fa-instagram follow-instagram"></i>
                            </a>
                            <a href="https://www.youtube.com/channel/UCUutW1j5c_ujoI9DH7FTYsQ" target="_blank" rel="noopener noreferrer">
                            <i class="fab fa-youtube follow-youtube"></i>
                            </a>
                            <a href="https://twitter.com/uiibf" target="_blank" rel="noopener noreferrer">
                            <i class="fab fa-twitter follow-twitter"></i>
                            </a>
                        </div>
                    </div>

                    <div className="bottom-bar">
                        <ul className={click ? 'nav-menu resp' : 'nav-menu'}>
                            <li><Link className={"bot-link"} to={"/kompetisi"}>Kompetisi</Link></li>
                            <li><Link className={"bot-link"} to={"/"}>Acara</Link></li>
                            <li><Link className={"bot-link"} to={"/"}>Book Fair</Link></li>
                            <li><Link className={"bot-link"} to={"/"}>Kontak</Link></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
    )
}