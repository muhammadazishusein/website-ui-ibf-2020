import React, { useState } from 'react';
import { Link } from "react-router-dom";
import logo from "../../image/logo.png";
import "./navbar.css";

export default function Navbar (props) {
    const [click] = useState(false)
    
    return(
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <Link to={"/"}><img alt="logoibf" src={logo} className="navbar-logo navbar-brand"/></Link>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className={click ? 'navbar-nav ml-auto resp' : 'navbar-nav ml-auto'}>
                    <li className="nav-item"><Link className={"nav-link " + props.beranda} to={"/"}>Beranda</Link></li>
                    <li className="nav-item"><Link className={"nav-link " + props.kompetisi} to={"/kompetisi"}>Kompetisi</Link></li>
                    <li className="nav-item"><Link className={"nav-link " + props.acara} to={"/acara"}>Acara</Link></li>
                    {/* <li className="nav-item"><Link className={"nav-link " + props.acara} to={"/"}>Acara</Link></li>
                    <li className="nav-item"><Link className={"nav-link " + props.bookfair} to={"/"}>Book Fair</Link></li> */}
                    {/* <li className="nav-item"><Link className={"nav-link " + props.kontak} to={"/"}>Kontak</Link></li> */}
                </ul>
            </div>
        </nav>
    )
}