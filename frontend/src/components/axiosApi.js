import axios from 'axios'

if (window.location.origin === "http://localhost:3000") {
  axios.defaults.baseURL = "http://127.0.0.1:8000";
} else {
  axios.defaults.baseURL = "https://ui-ibf-20.herokuapp.com/";
}

axios.defaults.xsrfCookieName = 'csrftoken'
axios.defaults.xsrfHeaderName = 'X-CSRFTOKEN'
const axiosInstance = axios.create({
    credentials: 'include',
    baseURL: 'https://www.uiibf.com/api/',
    timeout: 5000,
    headers: {
        'Content-Type': 'application/json',
        'accept': 'application/json'
    }
})

export default axiosInstance;
