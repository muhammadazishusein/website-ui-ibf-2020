import React from 'react';
// import { Link } from "react-router-dom";
import "./writing-academy.css";
import Navbar from '../navbar/navbar';
import logo_cerpen from "../../image/cerpen-vektor.png";
import gambar from "../../image/gambar-vektor.png";
import esai from "../../image/esai-vektor.png";
import foto from "../../image/foto-vektor.png";
import poster from "../../image/poster-vektor.png";
import puisi from "../../image/puisi-vektor.png";
import mic from "../../image/microphone-vektor.png";
// import hadroh from "../../image/hadroh.png";
// import hadiah_cerpen from "../../image/cerpen-hadiah.png";
import Carousel from "../kompetisi/lomba-carousel"; 

export default function writingAcademy() {

    const lombaList = [
        {
            foto: gambar,
            kategori: 'Drawing Academy',
            link:"drawing-academy"
        },
        {
            foto: foto,
            kategori: 'Photo Contest',
            link:"photo-contest"
        },
        {
            foto: poster,
            kategori: 'Creative Design',
            link:"creative-design"
        },
        {
            foto: puisi,
            kategori: 'In Words',
            link:"in-words"
        },
        {
            foto: logo_cerpen,
            kategori: 'In Story',
            link:"in-story"
        },
        // {
        //     foto: esai,
        //     kategori: 'Writing Academy',
        //     link:"writing-academy"
        // },
        {
            foto: mic,
            kategori: 'Voice',
            link:"voice"
        },
      ];

    return (
        <div>
            <Navbar
                kompetisi = "active"
            />
            <div className="container-fluid">
                <div className="row justify-content-center" id="cont-banner">
                    <div className="col-12" id="banner">

                        <div className="row mt-5 d-flex justify-content-center">
                            <div>
                                <img alt="logo" src={esai} className="img-fluid float-right"></img>
                            </div>
                            <div className="mx-5 my-3">
                                <div className="row text-center">
                                    <div className="col-12">
                                        <h6 className="text-white">Kompetisi Online UIIBF 2020</h6>
                                        <h1 className="text-white">UI IBF Writing Academy</h1>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row justify-content-center">
                            <div className="col-8">
                                <p className="text-center text-white" id="deskripsi-banner">
                                Lomba kepenulisan essay berisi argumentasi 
                                penulis berdasarkan hasil olah data yang dilakukan 
                                dengan disertai dengan data-data pendukung yang 
                                diperlukan. Kepenulisan essay memberikan kebebasan 
                                bagi penulis untuk berekspresi yang dihantarkan 
                                dalam bentuk tulisan atau melihat suatu topik 
                                dari sudut pandang penulis.
                                </p>
                            </div>
                        </div>
                        <div className="row justify-content-center mt-3 mb-5">
                            <div className="col-4 text-center">
                                <a href="/kompetisi/writing-academy/registrasi" className="btn btn-block btn-success text-white">Daftar</a>
                            </div>
                            {/* <div className="col-6"> */}
                            {/* </div> */}
                        </div>
                        {/* <div className="row justify-content-center mt-3 mb-5">
                            <div className="col-4 text-center">
                                <a href="/todo" className="btn btn-block btn-warning">Kontak Kami</a>
                            </div>
                        </div> */}
                    </div>
                </div>

                <div className="row mt-5 mb-2 justify-content-center" id="cont-timeline">
                    <div className="col-10">
                        <h3>Timeline</h3>
                        <ul>
                            <li>Pendaftaran : 15 November 2020 – 28 November 2020</li>
                            <li>Batas pengumpulan karya : 28 November 2020</li>
                            <li>Pengumuman : 27 Desember 2020 (saat acara puncak 6th UIIBF)</li>
                        </ul>
                    </div>
                </div>

                <div className="row my-2 justify-content-center text-justify" id="cont-syaratketentuan">
                    <div className="col-10">
                        <h3>Tema</h3>
                        <ul>
                            <li>Mengubah cara pandang seseorang terhadap Islam melalui tulisan.</li>
                            <li>Mendukung Indonesia Emas 2045 dengan literasi.</li>
                            <li>Menjaga kesehatan mental di masa pandemi COVID-19 melalui tulisan.</li>
                            <li>Dakwah kreatif penyebaran Islam melalui media digital.</li>
                            <li>Mengubah dunia melalui tulisan.</li>
                            <li>Menjaga ketahanan diri di tengah kondisi sulit seperti pada masa pandemi.</li>
                        </ul>
                    </div>
                </div>

                <div className="row my-2 justify-content-center text-justify" id="cont-syaratketentuan">
                    <div className="col-10">
                        <h3>Syarat & Ketentuan</h3>
                        <ol>
                            <li>Naskah yang dikirimkan adalah karya asli dan belum pernah dipublikasikan serta belum pernah diikutsertakan dalam perlombaan manapun.</li>
                            <li>Naskah esai terdiri atas 3-7 halaman.</li>
                            <li>Naskah esai disertai dengan referensi yang berusia maksimal sepuluh tahun.</li>
                            <li>Judul esai bebas sesuai dengan tema yang telah ditentukan, ekspresif dan menggambarkan isi dari esai.</li>
                            <li>Kepenulisan menggunakan bahasa Indonesia yang baik dan benar sesuai dengan Pedoman Umum Ejaan Bahasa Indonesia (PUEBI).</li>
                            <li>Naskah yang telah dikirimkan menjadi hak panitia sepenuhnya dan dapat dipublikasikan dengan tetap mencantumkan identitas penulis.</li>
                            <li>Naskah esai dapat disertai dengan gambar, tabel, atau grafik yang dapat membantu memperjelas isi esai.</li>
                            <li>Naskah esai terdiri atas tiga bagian yang tidak perlu disebutkan secara eksplisit, yaitu bagian pendahuluan, isi dan kesimpulan.</li>
                            <li>Peserta wajib melampirkan lembar pernyataan orisinalitas bersamaan dengan pengiriman karya</li>
                            <li>Naskah dikirimkan dalam bentuk PDF dengan format judul file “Esai_(nama peserta)_(judul esai)”</li>
                            <li>Naskah dikirimkan ke email: kompetisiuiibf@gmail.com dengan format judul email “Pengiriman karya_esai_(nama peserta)_(judul esai)”</li>
                        </ol>
                    </div>
                </div>

                <div className="row my-2 justify-content-center text-justify" id="cont-syaratketentuan">
                    <div className="col-10">
                        <h3>Sistematika Penulisan</h3>
                        <ol>
                            <li>Esai diketik dengan jenis font Times New Roman (TNR) dengan ukuran font 12 disertai dengan spasi 1.5 cm.</li>
                            <li>Esai diketik dalam kertas A4, dengan margin kiri 4 cm, dan margin atas, kanan dan bawah 3 cm.</li>
                            <li>Esai diketik dalam bentuk rata kanan-kiri (justify).</li>
                            <li>Judul esai diletakkan di tengah dengan jenis font Times New Roman berukuran 14 point.</li>
                            <li>Identitas penulis dituliskan pada bagian bawah judul dengan format: Nama, Instansi.</li>
                        </ol>
                    </div>
                </div>

                <div className="row mt-2 mb-5 justify-content-center text-justify" id="cont-kriteria">
                    <div className="col-10">
                        <h3>Kriteria Penilaian</h3>
                        <ul>
                            <li>Ketajaman analisa permasalahan (25%)</li>
                            <li>Urgensi dari permasalahan yang diangkat (15%)</li>
                            <li>Tata bahasa Indonesia dan sistematika kepenulisan (10%)</li>
                            <li>Kesesuaian tema (10%)</li>
                            <li>Argumentasi ide/gagasan (40%)</li>
                        </ul>
                    </div>
                </div>

                <div className="row py-5 d-flex justify-content-center bg-light">
                    <div className="mx-3">
                        <div className="row align-items-center">
                            <div className="col-12">
                                <h3 className="font-weight-bold text-center">Hadiah Kompetisi</h3>
                                <div className="row d-flex justify-content-center">
                                    <div className="ml-3 mr-3">
                                        <p>Pemenang akan mendapatkan</p>
                                        <ul>
                                            <li>Uang Pembinaan</li>
                                            <li>E-Sertifikat</li>
                                        </ul>
                                    </div>
                                    <div className="">
                                        <p>Peserta akan mendapatkan</p>
                                        <ul>
                                            <li>E-Sertifikat peserta</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <div className="mx-5">
                        <img alt="hadiah cerpen" src={hadiah_cerpen} className="img-fluid float-right"></img>
                    </div> */}
                </div>

                <div className="row py-5 d-flex justify-content-center">
                    <div className="mx-3">
                        <div className="row">
                            <div className="col-12" id="yukmenang">
                                <h4 className="font-weight-bold">Tertarik untuk jadi Pemenang?</h4>
                                <h4>Segera daftarkan dirimu sekarang!</h4>
                            </div>
                        </div>
                    </div>
                    <div className="mx-5 row d-flex align-items-center">
                        <div className="col-12">
                            <a href="/kompetisi/writing-academy/registrasi" className="mx-2 btn btn-success">Daftar</a>
                            {/* <a href="/todo" className="mx-2 btn btn-outline-dark">Kontak Kami</a> */}
                        </div>
                    </div>
                </div>

                <div className="row px-5 py-5">
                    <div className="col-12">
                        <div className="row mb-3">
                            <div className="col-12">
                                <h4 className="font-weight-bold text-center">Lomba Lainnya</h4>
                            </div>
                        </div>
                        <div className="slide">
                            <div hidden>
                                Icons made by 
                                <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a>
                                from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
                            </div>
                            <div className="row align-items-center">
                                {lombaList.map((lombaItem) => {
                                    return (
                                        <Carousel
                                            foto = {lombaItem.foto}
                                            kategori = {lombaItem.kategori}
                                            abu = {false}
                                            link = {lombaItem.link}
                                        />
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}