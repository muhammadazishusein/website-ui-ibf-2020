import React from 'react';
// import { Link } from "react-router-dom";
import "./creative-design.css";
import Navbar from '../navbar/navbar';
import logo_cerpen from "../../image/cerpen-vektor.png";
import gambar from "../../image/gambar-vektor.png";
import esai from "../../image/esai-vektor.png";
import foto from "../../image/foto-vektor.png";
import poster from "../../image/poster-vektor.png";
import puisi from "../../image/puisi-vektor.png";
import mic from "../../image/microphone-vektor.png";
// import hadroh from "../../image/hadroh.png";
// import hadiah_cerpen from "../../image/cerpen-hadiah.png";
import Carousel from "../kompetisi/lomba-carousel"; 

export default function creativeDesign() {

    const lombaList = [
        {
            foto: gambar,
            kategori: 'Drawing Academy',
            link:"drawing-academy"
        },
        {
            foto: foto,
            kategori: 'Photo Contest',
            link:"photo-contest"
        },
        // {
        //     foto: poster,
        //     kategori: 'Creative Design',
        //     link:"creative-design"
        // },
        {
            foto: puisi,
            kategori: 'In Words',
            link:"in-words"
        },
        {
            foto: logo_cerpen,
            kategori: 'In Story',
            link:"in-story"
        },
        {
            foto: esai,
            kategori: 'Writing Academy',
            link:"writing-academy"
        },
        {
            foto: mic,
            kategori: 'Voice',
            link:"voice"
        },
      ];

    return (
        <div>
            <Navbar
                kompetisi = "active"
            />
            <div className="container-fluid">
                <div className="row justify-content-center" id="cont-banner">
                    <div className="col-12" id="banner">

                        <div className="row mt-5 d-flex justify-content-center">
                            <div>
                                <img alt="logo" src={poster} className="img-fluid float-right"></img>
                            </div>
                            <div className="mx-5 my-3">
                                <div className="row text-center">
                                    <div className="col-12">
                                        <h6 className="text-white">Kompetisi Online UIIBF 2020</h6>
                                        <h1 className="text-white">UI IBF Creative Design</h1>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row justify-content-center">
                            <div className="col-8">
                                <p className="text-center text-white" id="deskripsi-banner">
                                Lomba poster pada kegiatan UI Islamic Book Fair 
                                bertujuan untuk memberi wadah kepada seseorang 
                                untuk menyalurkan kreativitasnya dalam membuat 
                                poster. Lomba ini juga dapat mengedukasi 
                                masyarakat akan pentingnya literasi.
                                </p>
                            </div>
                        </div>
                        <div className="row justify-content-center mt-3 mb-5">
                            <div className="col-4 text-center">
                                <a href="/kompetisi/creative-design/registrasi" className="btn btn-block btn-success text-white">Daftar</a>
                            </div>
                            {/* <div className="col-6"> */}
                            {/* </div> */}
                        </div>
                        {/* <div className="row justify-content-center mt-3 mb-5">
                            <div className="col-4 text-center">
                                <a href="/todo" className="btn btn-block btn-warning">Kontak Kami</a>
                            </div>
                        </div> */}
                    </div>
                </div>

                <div className="row mt-5 mb-2 justify-content-center" id="cont-timeline">
                    <div className="col-10">
                        <h3>Timeline</h3>
                        <ul>
                            <li>Pendaftaran : 15 November 2020 – 28 November 2020</li>
                            <li>Batas pengumpulan karya : 28 November 2020</li>
                            <li>Pengumuman : 27 Desember 2020 (saat acara puncak 6th UIIBF)</li>
                        </ul>
                    </div>
                </div>

                <div className="row my-2 justify-content-center text-justify" id="cont-syaratketentuan">
                    <div className="col-10">
                        <h3>Tema</h3>
                        <ul>
                            <li>“Melihat Dunia Melalui Buku”</li>
                            <li>"Mengubah Cara Pandang Seseorang terhadap Islam Melalui Tulisan"</li>
                            <li>"Menjadi Manusia Tangguh di Masa Kini Sesuai Ajaran Islam"</li>
                            <li>"Tingkatkan Literasi, Wujudkan Generasi Cerdas"</li>
                            <li>"Serba-serbi Covid-19 dalam Pandangan Islam"</li>
                        </ul>
                    </div>
                </div>

                <div className="row my-2 justify-content-center text-justify" id="cont-syaratketentuan">
                    <div className="col-10">
                        <h3>Syarat & Ketentuan</h3>
                        <ol>
                            <li>Peserta mengumpulkan karya berupa poster yang sesuai dengan tema dan ketentuan kompetisi</li>
                            <li>Poster yang dikirimkan merupakan hasil kreativitas yang dikemas secara kreatif, interaktif secara ORISINIL dan belum pernah dipublikasikan maupun diperlombakan sebelumnya di perlombaan melalui media manapun</li>
                            <li>Teknik dan gaya penulisan poster bebas, orisinil dan tidak memakai elemen yang melanggar hak cipta (bukan tiruan atau jiplakan)</li>
                            <li>Poster dibuat dengan menggunakan aplikasi komputer atau software yang sudah umum (Corel Draw, Photoshop, atau kombinasi aplikasi lain)</li>
                            <li>Poster dibuat berukuran A3</li>
                            <li>Jumlah warna bebas</li>
                            <li>Poster ditulis dalam Bahasa Indonesia yang baik dan benar sesuai dengan ejaan yang disempurnakan</li>
                            <li>Isi poster memuat informasi edukatif yang mudah dipahami dan dikemas secara menarik</li>
                            <li>Karya dikirimkan dalam bentuk JPG dengan format judul file “Poster_(nama peserta)_(judul poster)”</li>
                            <li>Naskah dikirimkan ke email: kompetisiuiibf@gmail.com dengan format judul email “Pengiriman karya_Poster_(nama peserta)_(judul poster)”</li>
                            <li>Hasil karya juga diupload di instagram masing-masing, wajib tag akun instagram @salam_ui dan @uiibf serta menggunakan tagar/hashtag #LombaPosteruiibf6 #LombaPosteruiibf6 #uiibf6</li>
                            <li>Akun instagram tidak dalam kondisi private sampai pengumuman (27 Desember 2020)</li>
                            <li>Jumlah like pada postingan instagram memengaruhi hasil akhir penilaian.</li>
                            <li>9 poster terbaik peserta akan dipublikasikan di instagram @uiibf untuk memperebutkan juara favorit yang diperoleh dari jumlah like</li>
                        </ol>
                    </div>
                </div>

                <div className="row mt-2 mb-5 justify-content-center text-justify" id="cont-kriteria">
                    <div className="col-10">
                        <h3>Kriteria Penilaian</h3>
                        <ul>
                            <li>Kesesuaian dengan tema</li>
                            <li>Orisinalitas poster</li>
                            <li>Mudah dilihat</li>
                            <li>Menarik</li>
                            <li>Terstruktur dan informatif</li>
                        </ul>
                    </div>
                </div>

                <div className="row py-5 d-flex justify-content-center bg-light">
                    <div className="mx-3">
                        <div className="row align-items-center">
                            <div className="col-12">
                                <h3 className="font-weight-bold text-center">Hadiah Kompetisi</h3>
                                <div className="row d-flex justify-content-center">
                                    <div className="ml-3 mr-3">
                                        <p>Pemenang akan mendapatkan</p>
                                        <ul>
                                            <li>Uang Pembinaan</li>
                                            <li>E-Sertifikat</li>
                                        </ul>
                                    </div>
                                    <div className="">
                                        <p>Peserta akan mendapatkan</p>
                                        <ul>
                                            <li>E-Sertifikat peserta</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <div className="mx-5">
                        <img alt="hadiah cerpen" src={hadiah_cerpen} className="img-fluid float-right"></img>
                    </div> */}
                </div>

                <div className="row py-5 d-flex justify-content-center">
                    <div className="mx-3">
                        <div className="row">
                            <div className="col-12" id="yukmenang">
                                <h4 className="font-weight-bold">Tertarik untuk jadi Pemenang?</h4>
                                <h4>Segera daftarkan dirimu sekarang!</h4>
                            </div>
                        </div>
                    </div>
                    <div className="mx-5 row d-flex align-items-center">
                        <div className="col-12">
                            <a href="/kompetisi/creative-design/registrasi" className="mx-2 btn btn-success">Daftar</a>
                            {/* <a href="/todo" className="mx-2 btn btn-outline-dark">Kontak Kami</a> */}
                        </div>
                    </div>
                </div>

                <div className="row px-5 py-5">
                    <div className="col-12">
                        <div className="row mb-3">
                            <div className="col-12">
                                <h4 className="font-weight-bold text-center">Lomba Lainnya</h4>
                            </div>
                        </div>
                        <div className="slide">
                            <div hidden>
                                Icons made by 
                                <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a>
                                from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
                            </div>
                            <div className="row align-items-center">
                                {lombaList.map((lombaItem) => {
                                    return (
                                        <Carousel
                                            foto = {lombaItem.foto}
                                            kategori = {lombaItem.kategori}
                                            abu = {false}
                                            link = {lombaItem.link}
                                        />
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}