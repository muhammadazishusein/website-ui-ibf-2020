import React from 'react';

export default function MiniLomba(props) {
    return(
        <div className="card-lomba">
            <div className="mini-lomba">
                <img alt="foto lomba" src={props.foto}></img>
                <h5>{props.kategori}</h5>
                <p>{props.deskripsi}</p>
                <a href={"/kompetisi/" + props.link}><p> See More{">"}</p></a>
            </div>
            <br></br>
        </div>
    );
}