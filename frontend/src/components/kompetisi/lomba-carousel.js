import React from 'react';

export default function Carousel(props) {
    return(
        <div className="col-auto">
            <a href={"/kompetisi/" + props.link} style={{color: "inherit"}}>
                <div className="row card-box justify-content-center">
                    <div className="carousel-card">
                        <div className="row justify-content-center">
                            <img alt="foto lomba" src={props.foto}></img>
                        </div>
                        <br></br>
                        <div className="row justify-content-center text-center">
                            <h4 className="text-center font-weight-bold">{props.kategori}</h4>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    );
}