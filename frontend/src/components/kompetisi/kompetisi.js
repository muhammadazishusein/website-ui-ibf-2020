import React from 'react';
import "../../components/kompetisi/kompetisi.css";
import Navbar from "../../components/navbar/navbar";
import MiniLomba from "./mini-lomba"; 
import Carousel from "./lomba-carousel"; 
// import assets"
import cerpen from "../../image/cerpen-vektor.png";
import gambar from "../../image/gambar-vektor.png";
import esai from "../../image/esai-vektor.png";
import foto from "../../image/foto-vektor.png";
import poster from "../../image/poster-vektor.png";
import puisi from "../../image/puisi-vektor.png";
import mic from "../../image/microphone-vektor.png";
import hadroh from "../../image/hadroh.png";
import elipsKiri from "../../image/elips-kiri.png";
import elipsKanan from "../../image/elips-kanan.png";


export default function kompetisi() {

    const lombaList = [
        {
            foto: gambar,
            kategori: 'Drawing Academy',
            deskripsi: "UI IBF Drawing Academy adalah lomba menggambar yang diperuntukkan bagi siswa/i SD/sederajat kelas 3 hingga kelas 6. Lomba ini diselenggarakan untuk memberikan kesempatan kepada adik-adik Sekolah Dasar untuk dapat ikut serta mensyiarkan nilai-nilai keislaman melalui gambar.",
            link:"drawing-academy"
        },
        {
            foto: foto,
            kategori: 'Photo Contest',
            deskripsi: "Lomba fotografi merupakan salah satu lomba yang akan meramaikan kegiatan UI Islamic Book Fair dan juga lomba untuk menggali minat dan bakat dalam melukis menggunakan media cahaya sehingga memiliki nilai estetika dan seni bagi yang melihatnya.",
            link:"photo-contest"
        },
        {
            foto: poster,
            kategori: 'Creative Design',
            deskripsi: "Lomba poster pada kegiatan UI Islamic Book Fair bertujuan untuk memberi wadah kepada seseorang untuk menyalurkan kreativitasnya dalam membuat poster. Lomba ini juga dapat mengedukasi masyarakat akan pentingnya literasi.",
            link:"creative-design"
        },
        {
            foto: puisi,
            kategori: 'In Words',
            deskripsi: "Puisi merupakan salah satu bentuk karya sastra yang diungkapkan dengan bantuan kata-kata kiasan. Puisi merupakan hasil ungkapan perasaan yang dituliskan penulis dengan untaian gaya bahasa yang sesuai dengan ciri penulis.",
            link:"in-words"
        },
        {
            foto: cerpen,
            kategori: 'In Story',
            deskripsi: "Cerita pendek adalah sebuah karya naratif yang cenderung fiktif dan padat pada penyampaian isi cerita. UI IBF menyelenggarakan kompetisi cerita pendek dengan harapan mampu berdakwah melalui kisah-kisah yang inspiratif dan bernilai islam.",
            link:"in-story"
        },
        {
            foto: esai,
            kategori: 'Writing Academy',
            deskripsi: "Lomba kepenulisan essay berisi argumentasi penulis berdasarkan hasil olah data yang dilakukan dengan disertai dengan data-data pendukung yang diperlukan. Kepenulisan essay memberikan kebebasan bagi penulis untuk berekspresi yang dihantarkan dalam bentuk tulisan atau melihat suatu topik dari sudut pandang penulis.",
            link:"writing-academy"
        },
        {
            foto: mic,
            kategori: 'Voice',
            deskripsi: "UI IBF Voice merupakan salah satu lomba yang bertujuan untuk memberi wadah kepada seseorang untuk menyalurkan kreativitasnya serta menggali minat dan bakat dalam bidang bernyanyi dan bermusik bernuansa islam yang khusus untuk laki-laki.",
            link:"voice"
        },
      ];

    return (
        <div className="kerak">
            <Navbar
                kompetisi = "active"
            />
            <div className="container-isi">
                <div className="page-1">

                    {/* Cover bagian atas */}
                    <div className="row">
                        <div className="col-12">
                            <div className="bg-page-1">
                                {/* <img src={background}></img> */}
                                <h5>UIIBF 2020 Presents:</h5>
                                <h1 style={{fontSize: 52 + "pt"}}>Kompetisi</h1>
                                {/* <h6>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lacinia consectetur 
                                    semper. Vestibulum metus orci, venenatis non nulla id, luctus dapibus ligula</h6> */}
                            </div>
                        </div>
                    </div>
                </div>

                {/* Ketentuan Lomba */}
                <div className="row mt-5 mb-2 justify-content-center text-justify" style={{width: 100 + "%"}}>
                    <div className="col-10">
                        <h3>Ketentuan Umum</h3>
                        <p className="mb-2">Ketentuan umum merupakan hal-hal yang berlaku untuk setiap cabang lomba. Berikut ketentuan umum dari Kompetisi 6th UIIBF:</p>
                        <ol>
                            <li>Lomba bersifat perorangan sehingga peserta tidak dibantu oleh pihak manapun</li>
                            <li>Lomba bersifat umum, yaitu terbuka bagi setiap rentang usia (kecuali Drawing Academy)</li>
                            <li>Setiap karya yang dikirimkan adalah hasil buatan SENDIRI. Panitia tidak bertanggung jawab apabila ada tuntutan terhadap peserta terkait hak cipta</li>
                            <li>Karya yang dibuat sesuai dengan tema yang disediakan pada setiap cabang lomba</li>
                            <li>Karya yang dibuat tidak mengandung SARA serta unsur pornografi</li>
                            <li>Karya tidak merugikan pihak manapun</li>
                            <li>Setiap peserta hanya diperbolehkan mengirimkan satu karya di setiap cabang lomba</li>
                            <li>Setiap orang boleh mengikuti lebih dari satu cabang lomba dengan cara mendaftar pada link pendaftaran secara satu per satu</li>
                            <li>Peserta yang mendaftar bersedia berinfaq untuk berjalannya kegiatan syiar terbesar di Universitas Indonesia. Nominal dan rekening tujuan dapat dilihat ketika melakukan pendaftaran</li>
                            <li>Peserta yang telah menyelesaikan proses pendaftaran akan diundang ke dalam Grup peserta</li>
                            <li>Setiap ketentuan wajib ditaati oleh seluruh peserta. Pelanggaran atas ketentuan akan berakibat pada pengurangan nilai hingga diskualifikasi</li>
                            <li>Hasil penilaian juri tidak dapat diganggu gugat</li>
                            <li>Panitia berhak mengubah dan menambah ketentuan dengan pemberitahuan kepada seluruh peserta melalui Grup peserta lomba</li>
                        </ol>
                    </div>
                </div>

                {/* List Lomba */}
                <div className="page-2">
                    <br></br>
                    <h3>Daftar Lomba</h3>
                    <div hidden>
                        Icons made by 
                        <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a>
                        from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
                    </div>
                    <div className="list-lomba">
                        {lombaList.map((lombaItem) => {
                            return (
                                <MiniLomba
                                    foto = {lombaItem.foto}
                                    kategori = {lombaItem.kategori}
                                    deskripsi = {lombaItem.deskripsi}
                                    link = {lombaItem.link}
                                />
                            );
                        })}
                    </div>  
                    <br></br>
                    <br></br>
                    <br></br>
                </div>

                {/* Tentang Kopmpetisi UIIBF */}
                <div className="py-5 page-3">
                    <div className="row py-5 d-flex justify-content-center page-3">
                        <div className="mx-3">
                            <div className="row my-auto">
                                <div className="col-12">
                                    <div className="text-kiri mx-5">
                                        <h3 className="font-weight-bold font-smaller mb-4">Tentang Kompetisi UIIBF</h3>
                                        <h4>
                                            Ikuti berbagai kompetisi yang 
                                            diselenggarakan UIIBF dengan hadiah 
                                            yang menarik dan juga kesempatan 
                                            mengembangkan diri.
                                        </h4>
                                        <ul>
                                            <li><h4>Bisa mendapatkan pahala</h4></li>
                                            <li><h4>Kesempatan mengembangkan diri</h4></li>
                                            <li><h4>Bisa mendapatkan hadiah menarik</h4></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="mx-5">
                            <img alt="orang sedang hadroh" src={hadroh} className="img-fluid float-right"></img>
                        </div>
                    </div>
                </div>

                {/* Page Terakhir */}
                <div className="page-4 pt-3">
                    <div className="bungkus-quotes justify-content-center">
                        <div>
                            <img alt="elips bagian kiri" src={elipsKiri} className="img-fluid"></img>
                        </div>
                        <h5 className="font-italic">“Competition is always a good thing. It forces us to do our best. A monopoly 
                            renders people complacent and satisfied with mediocrity” -- Nancy Pearcy</h5>
                        <div>
                            <img alt="elips bagian kanan" src={elipsKanan} className="img-fluid"></img>
                        </div>
                    </div>

                    <div className="row mx-5 pb-5">
                        <div className="col-12">
                            <div className="row">
                                <div className="col-12 mb-5">
                                    <h3 className="font-weight-bold text-center">Sudah Siap menjadi Pemenang?</h3>
                                </div>
                            </div>
                            <div className="slide">
                                <div className="row">
                                    {lombaList.map((lombaItem) => {
                                        return (
                                            <Carousel
                                                foto = {lombaItem.foto}
                                                kategori = {lombaItem.kategori}
                                                link = {lombaItem.link}
                                            />
                                        );
                                    })}
                                </div>
                            </div>
                        </div>
                    </div>


                    {/* <div className="dah-siap">
                        <h3>Sudah Siap menjadi Pemenang?</h3>
                    </div>
                    <div className="carousel">
                        {lombaList.map((lombaItem) => {
                            return (
                                <Carousel
                                    foto = {lombaItem.foto}
                                    kategori = {lombaItem.kategori}
                                />
                            );
                        })}
                    </div> */}
                    <br></br>
                    <br></br>
                    <br></br>
                </div>
            </div>

        </div>
    )
}
