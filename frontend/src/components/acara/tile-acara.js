import "./acara.css";
import React from 'react';

export default function TileAcara(props) {
    let linkRegis;
    if(props.jenis === "Bedah Buku" || props.judul === "Inspiring Talk 1: Self Development" || props.judul === "Workshop 1"){
        linkRegis = (<a href="/acara" className="btn btn-danger">Pendaftaran Ditutup</a>);
    }
    else{
        linkRegis = (<a href={props.linkDaftar} className="btn btn-success">Daftar</a>);
    }

    return(
        <div className="tile-acara">
            <div className={props.background}>
                
            </div>

            <div className="keterangan">
                <h4>{props.jenis}</h4>
                
                <h2>{props.judul}</h2>

                <i class="fas fa-map-marker-alt"></i>
                <h6>{props.tempat}</h6>
                <br/>
                <i class="far fa-clock"></i>
                <h6>{props.jam}</h6>
                <br/>
                <i class="fas fa-calendar-day"></i>
                <h6>{props.tanggal}</h6>

                <div className="button-keterangan">
                    <a href={props.linkDetail} className="btn btn-outline-success">Lihat</a>
                    {/* <a href={props.linkDaftar} className="btn btn-success">Daftar</a> */}
                    {linkRegis}
                </div>
            </div>
        </div>
    )
}