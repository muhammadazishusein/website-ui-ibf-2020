import React from 'react';
import Navbar from "../../components/navbar/navbar";
import "./detailAcara.css";
import buletkanan from "../../image/Paket3Kanan.png";
import buletkiri from "../../image/Paket3Kiri.png";

export default function DetailAcara(props) {
    return (
        <div className="tampungan">
            <Navbar
                acara = "active"
            />

            <div className="header-detail-acara w-100">
                <div className="konten-header">
                    <h5>{props.jenis}</h5>

                    <h1>{props.namaAcara}</h1>

                    <h6>Pembicara</h6>
                </div>
            </div>

            <div className="detailed-acara">
                <div className="detail-atas">
                    <div className="penyelenggaraan-box">
                        <div className="konten-penyelenggaraan">
                            <h3>Penyelenggaraan</h3>

                            <i class="fas fa-map-marker-alt"></i>
                            <h6>{props.tempat}</h6>
                            <br/>
                            <i class="far fa-clock"></i>
                            <h6>{props.jam}</h6>
                            <br/>
                            <i class="fas fa-calendar-day"></i>
                            <h6>{props.tanggal}</h6>
                        </div>
                    </div>

                    <div className="penyelenggaraan-box">
                        <div className="konten-penyelenggaraan">
                            <h3>Pendaftaran Partisipan</h3>

                            <p>Siap untuk memperkaya ilmu yang dimiliki dengan ikut seminar ini? Ayo segera daftarkan diri anda!</p>

                            <a href={props.link} className="btn btn-success">Daftar</a>
                        </div>
                    </div>
                </div>

                <div className="detail-bawah">
                    <h3>Tentang Acara</h3>

                    <hr/>

                    <p>{props.deskripsi}</p>

                    <hr/>

                    <h3>Pembicara</h3>
                    <div className="list-pembicara">
                        {props.listPembicara.map(
                            (pembicara) => {
                                return(
                                    <div className="card-pembicara">
                                        <div className={pembicara.fotoBesar}></div>

                                        <div className="keterangan-pembicara">
                                            <h4>{pembicara.namaPembicara}</h4>
                                            <h5>{pembicara.role}</h5>
                                        </div>
                                    </div>
                                );
                            }
                        )}
                    </div>

                    <hr/>

                    <h3>Benefit dari acara ini</h3>

                    <ul>
                    {props.benefits.map((benefit) => {
                            return (
                                <li>{benefit}</li>
                            );
                        })}
                    </ul>
                </div>
            </div>

            <div className="row w-100 justify-content-center mx-0 mt-5 align-items-center">
                <div className="col-2">
                        <img alt="telur bagian kiri" src={buletkiri} className="img-fluid" ></img>
                </div>
                <div className="col-8 text-center">
                    <h2 className=" my-auto flex-column" style={{fontWeight: "Bold"}}>
                        Ada Pertanyaan Seputar Acara UIIBF?
                    </h2>
                    <a className="btn btn-warning mt-5" href="https://www.instagram.com/uiibf/">Kontak Kami</a>
                </div>
                <div className="col-2 d-flex">
                        <img alt="telur bagian kanan" src={buletkanan} className="img-fluid ml-auto" ></img>
                </div>
            </div>

        </div>
    )
}