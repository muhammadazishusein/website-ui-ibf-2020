import React from 'react';
import Acara from './detailAcara';

export default function Workshop() {
    const listPembicara = [
        {
            'namaPembicara' : 'Lala Elmira',
            'role' : 'Moderator Workshop 1',
            'fotoKecil' : 'bg-lala',
            'fotoBesar' : 'bg-lala'
        },
        {
            'namaPembicara' : 'Aulia Qisthi',
            'role' : 'Pembicara Workshop 1',
            'fotoKecil' : 'bg-aulia',
            'fotoBesar' : 'bg-aulia'
        },
    ];

    const benefits = [
        'Pengetahuan mengenai cara tembus jurnal nasional/Internasional dan cara menulis abstrak dalam jurnal ilmiah',
        'Pengetahuan cara membuat CV yang profesional',
        'Mengetahui secara langsung interview kerja saat agenda berlangsung ',
        'Kesempatan mendapat Give away buku untuk pemenang terpilih',
        'Relasi dalam minat yang sama',
    ];

    return(
        <Acara
            jenis = "Workshop UI IBF"
            namaAcara =  "Workshop 1"
            listPembicara = {listPembicara}
            tempat = "Online Zoom"
            jam = "13.00 - 16.00"
            tanggal = "13 Desember 2020"
            link = "/acara/daftar/workshop"
            deskripsi = "Workshop adalah salah satu rangkaian acara UI Islamic Book Fair yang bertujuan untuk melatih skill  dan menambah pengetahuan peserta. Tahun ini, workshop UI IBF 6 mempersembahkan suatu pelatihan dalam penulisan jurnal ilmiah dan pelatihan CV dan wawancara dengan mendatangkan pembicara yang kompeten di bidangnya. "
            benefits = {benefits}
        />
    )
}