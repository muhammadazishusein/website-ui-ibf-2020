import React from 'react';
import Acara from './detailAcara';

export default function SelfDevelopment() {
    const listPembicara = [
        {
            'namaPembicara' : 'Indah Shafira Zata Dini, B.Soc.Sc., EdM.',
            'role' : 'Master of Education- Harvard University, Education Consultant at The World Bank',
            'fotoKecil' : 'bg-zata',
            'fotoBesar' : 'bg-zata'
        },
        {
            'namaPembicara' : 'Diajeng Lestari',
            'role' : 'Founder Hijup',
            'fotoKecil' : 'bg-diajeng',
            'fotoBesar' : 'bg-diajeng'
        },
        {
            'namaPembicara' : 'Meyda Sefira, S.T., M.T.',
            'role' : '',
            'fotoKecil' : 'bg-meyda',
            'fotoBesar' : 'bg-meyda'
        },
        {
            'namaPembicara' : 'Fatimah Syakura',
            'role' : 'Mahasiswa Berprestasi FKM UI 2020',
            'fotoKecil' : 'bg-fatimah',
            'fotoBesar' : 'bg-fatimah'
        },
    ];

    const benefits = [
        'Semangat untuk berprogres meraih mimpi',
        'Akses modul materi dan rekaman zoom ',
        'E-Certificate',
        'Menambah relasi',
    ];

    return(
        <Acara
            jenis = "Seminar dan Talkshow UI IBF"
            namaAcara =  "Inspiring Talk 1: Self Development Menjadi Manusia Yang Statis Atau Dinamis?" 
            listPembicara = {listPembicara}
            tempat = "Online Zoom"
            jam = "09.30 - 12.00"
            tanggal = "13 Desember 2020"
            link = "/acara/daftar/self-development"
            deskripsi = "Halo sobat literasi! Apakah kamu pernah atau sedang merasa ngga bisa apa-apa? Apakah kamu takut untuk bergerak dan pasrah dengan keadaan sekarang? Apakah kamu pernah bermimpi tinggi tapi kamu ngga tau cara mewujudkannya atau masih malas untuk merealisasikan mimpi kamu? Pernah ngga sih merasa bingung sama tujuan hidup dan apa aja yang harus dilakuin? Nahh melalui seminar talkshow seri 1 akan membahas mengenai pentingnya 'self development' yang akan membantu kita mengelola hidup dan membentuk individu muslim yang berkualitas, dinamis, dan progresif."
            benefits = {benefits}
        />
    )
}