import React from 'react';
import Navbar from "../../components/navbar/navbar";
import "./acara.css";
import Tile from "./tile-acara";
import telur3 from "../../image/telur-3-jejer.png";
import media from "../../image/Media.png"
import buletkanan from "../../image/Paket3Kanan.png";
import buletkiri from "../../image/Paket3Kiri.png";

export default function Acara() {
    const listAcara = [
        {
            backgroound: 'bg-acara-bedah-buku',
            listPembicara: [
                'Alvi Syahrin',
            ],
            jenis: 'Bedah Buku',
            judul: 'Tales From The Book',
            tempat: 'Online melalui Zoom',
            jam: '12.30 - 14.30',
            tanggal: '6 Desember 2020',
            link: '/acara/daftar/bedah-buku',
            link2: '/acara/bedah-buku',
        },
        {
            backgroound: 'bg-acara-self-dev',
            listPembicara: [
                'Sule',
                'Andre',
            ],
            jenis: 'Seminar & Talkshow',
            judul: 'Inspiring Talk 1: Self Development',
            tempat: 'Online melalui Zoom',
            jam: '09.30 - 12.00',
            tanggal: '13 Desember 2020',
            link: '/acara/daftar/self-development',
            link2: '/acara/self-development',
        },
        {
            backgroound: 'bg-acara-workshop',
            listPembicara: [
                'Sule',
                'Andre',
            ],
            jenis: 'Workshop',
            judul: 'Workshop 1',
            tempat: 'Online melalui Zoom',
            jam: '13.00 - 16.00',
            tanggal: '13 Desember 2020',
            link: '/acara/daftar/workshop-1',
            link2: '/acara/workshop-1',
        },
        {
            backgroound: 'bg-acara-workshop',
            listPembicara: [
                'Sule',
                'Andre',
            ],
            jenis: 'Workshop',
            judul: 'Workshop 2',
            tempat: 'Online melalui Zoom',
            jam: '13.00 - 16.00',
            tanggal: '20 Desember 2020',
            link: '/acara/daftar/workshop-2',
            link2: '/acara/workshop-2',
        },
        {
            backgroound: 'bg-acara-inovasi-teknologi',
            listPembicara: [
                'Sule',
                'Andre',
            ],
            jenis: 'Seminar dan Talkshow',
            judul: 'Inspiring Talk 2: Innovation & Technology',
            tempat: 'Online melalui Zoom',
            jam: '09.30 - 12.00',
            tanggal: '27 Desember 2020',
            link: '/acara/daftar/inovasi-teknologi',
            link2: '/acara/inovasi-teknologi',
        },
        // {
        //     backgroound: 'bg-acara',
        //     listPembicara: [
        //         'Sule',
        //         'Andre',
        //     ],
        //     jenis: 'Seminar dan Talkshow',
        //     judul: 'Self-Development',
        //     tempat: 'Online melalui Zoom',
        //     jam: '08.00 - 11.00',
        //     tanggal: '27 Desember 2020',
        //     link: '/acara/daftar/self-development',
        //     link2: '/acara/self-development',
        // },
    ];

    return (
        <div className="tampungan">
            <Navbar
                acara = "active"
            />
            
            <div className="page-1">

                    {/* Cover bagian atas */}
                    <div className="row w-100">
                        <div className="col-12">
                            <div className="bg-page-1">
                                {/* <img src={background}></img> */}
                                <h5>UIIBF 2020 Presents:</h5>
                                <h1 className="mx-2" style={{fontSize: 52 + "pt"}}>Acara UIIBF</h1>
                                <h6 className="mb-5">
                                    UIIBF 2020 menyelenggarakan beberapa acara 
                                    berupa seminar & talkshow, bedah buku dan 
                                    workshop untuk teman-teman yang ingin 
                                    memperkaya ilmu yang dimiliki.
                                </h6>
                                <br></br>
                                <div>
                                    <img className="img-fluid" style={{maxWidth: 268 + "px", maxHeight: 45 + "px"}} src={telur3} alt="Telur 3"></img>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            <div className="box-container">
                <h1>Daftar Acara</h1>

                <div className="listAcara">
                    {listAcara.map(
                        (acara) => {
                            return(
                                <Tile
                                    background = {acara.backgroound}
                                    jenis = {acara.jenis}
                                    listPembicara = {acara.listPembicara}
                                    judul = {acara.judul}
                                    tempat = {acara.tempat}
                                    jam = {acara.jam}
                                    tanggal = {acara.tanggal}
                                    linkDaftar = {acara.link}
                                    linkDetail = {acara.link2}
                                />
                            );
                        }
                    )}
                </div>
            </div>
            
            <div className="bg-ilmu w-100">
                <div className="konten-ilmu">
                    <div className="narasi-ilmu">
                        <h2>Keuntungan Menimba Ilmu</h2>

                        <p>Dengan mengikuti Acara Virtual UIIBF 2020, anda bisa menimba ilmu sehingga memperoleh banyak keuntungan sebagai berikut:</p>

                        <ul>
                            <li>Dapat pahala yang melimpah</li>
                            <li>Mendapat kepuasan duniawi</li>
                            <li>Mendapat rahmat dari Allah swt.</li>
                        </ul>
                    </div>

                    <div className="gambar-ilmu">
                        <img src={media} alt="dokumentasi"/>
                    </div>
                </div>
            </div>

            <div className="row w-100 justify-content-center mx-0 mt-5 align-items-center">
                <div className="col-2">
                        <img alt="telur bagian kiri" src={buletkiri} className="img-fluid" ></img>
                </div>
                <div className="col-8 text-center">
                    <h2 className=" my-auto flex-column" style={{fontWeight: "Bold"}}>
                        Ada Pertanyaan Seputar Acara UIIBF?
                    </h2>
                    <a className="btn btn-warning mt-5" href="https://www.instagram.com/uiibf/">Kontak Kami</a>
                </div>
                <div className="col-2 d-flex">
                        <img alt="telur bagian kanan" src={buletkanan} className="img-fluid ml-auto" ></img>
                </div>
            </div>

        </div>
    )
}