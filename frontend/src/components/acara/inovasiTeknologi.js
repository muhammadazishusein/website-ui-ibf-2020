import React from 'react';
import Acara from './detailAcara';

export default function InovasiTeknologi() {
    const listPembicara = [
        {
            'namaPembicara' : 'Shofwan Al Banna',
            'role' : 'Moderator',
            'fotoKecil' : 'bg-shofwan',
            'fotoBesar' : 'bg-shofwan'
        },
        {
            'namaPembicara' : 'Prof. Dr.-Ing. H. Fahmi Amhar',
            'role' : 'Pembicara',
            'fotoKecil' : 'bg-fahmi',
            'fotoBesar' : 'bg-fahmi'
        },
        {
            'namaPembicara' : 'Radyum Ikono, Ph.D.',
            'role' : 'Founder Schoters',
            'fotoKecil' : 'bg-radyum',
            'fotoBesar' : 'bg-radyum'
        },
    ];

    const benefits = [
        'Mendapat informasi mengenai perkembangan inovasi & teknologi serta pemanfaatannya secara efektif',
        'Akses modul materi dan rekaman zoom ',
        'E-Certificate',
        'Menambah relasi',
    ];

    return(
        <Acara
            jenis = "Seminar & Talkshow UI IBF"
            namaAcara =  "Inspiring Talk 2: Innovation & Technology Wujudkan Umat Yang Kuat Nan Tangguh Dengan Teknologi, Inovasi, Dan Islam"
            listPembicara = {listPembicara}
            tempat = "Online Zoom"
            jam = "09.30 - 12.00"
            tanggal = "27 Desember 2020"
            link = "/acara/daftar/inovasi-teknologi"
            deskripsi = "Halo sobat literasi! Perkembangan teknologi saat ini sangat maju dan inovasi di berbagai bidang pun beragam, namun apakah kita benar-benar tau mengenai perkembangan inovasi dan peran teknologi tersebut? Apakah kita sudah memanfaatkannya dengan benar dan maksimal khususnya bagi umat muslim? Melalui seminar talkshow seri 2 ini, kita membahas tentang inovasi dan teknologi yang sedang berkembang serta peran umat muslim didalamnya sebagai solusi dari permasalahan umat dan syiar Islam."
            benefits = {benefits}
        />
    )
}