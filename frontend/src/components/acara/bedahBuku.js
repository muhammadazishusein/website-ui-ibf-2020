import React from 'react';
import Acara from './detailAcara';

export default function BedahBuku() {
    const listPembicara = [
        {
            'namaPembicara' : 'Alvi Syahrin',
            'role' : 'Pembicara',
            'fotoKecil' : 'bg-alvi',
            'fotoBesar' : 'bg-alvi'
        },
    ];

    const benefits = [
        'Mendapatkan pengetahuan dalam mengatasi rasa insecure dalam diri',
        'Mendapatkan pengalaman berharga dari penulis.',
        'Mengetahui sudut pandang penulis dalam membuat sebuah karya berupa buku.',
        'Mendapatkan ilmu yang bermanfaat.',
        'Membangun rasa semangat dalam mencintai literasi.',
        'Membangun semangat dalam menulis.',
        'Akses modul materi dan rekaman zoom.',
    ];

    return(
        <Acara
            jenis = "Bedah Buku UI IBF"
            namaAcara =  "Tales From The Book: Menjelajah dengan Cerita, Ulurkan Tangan untuk Dunia"
            listPembicara = {listPembicara}
            tempat = "Online Zoom"
            jam = "12.30 - 14.30"
            tanggal = "6 Desember 2020"
            link = "/acara/daftar/bedah-buku"
            deskripsi = "Untuk kamu yang seringkali merasa lelah dengan suatu keadaan. Untuk kamu yang seringkali merasa penuh kekurangan. Untuk kamu yang merasa tidak pantas dicintai, dan untuk kamu yang sedang merasa tidak baik-baik saja. Kini saatnya untuk mulai mengenali diri dengan menemukan makna bahagia sesungguhnya. Bersama, di dalam kegiatan bedah buku kita akan temukan jawabannya dari buku bertajuk  “Jika Kita Tak Pernah Baik-Baik Saja”. Buku ke-3 karya Alvi Syahrin seorang penulis best seller top Gramedia di seluruh Indonesia yang akan hadir perdana di UI Islamic Bookfair. Buku ini mengupas tuntas tentang bagaimana kita dapat memahami makna self love atau mencintai diri sendiri. Maka jangan sampai lewatkan dan persiapkan diri untuk melepas rasa insecure dalam diri bersama UI Islamic Bookfair 2020."
            benefits = {benefits}
        />
    )
}