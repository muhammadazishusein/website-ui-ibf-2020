import React, { useState } from 'react';
import { Link } from "react-router-dom";
import "../../components/cerpen/cerpen.css";
import Navbar from '../navbar/navbar';
import logo_cerpen from "../../image/cerpen-vektor.png";
import gambar from "../../image/gambar-vektor.png";
import esai from "../../image/esai-vektor.png";
import foto from "../../image/foto-vektor.png";
import poster from "../../image/poster-vektor.png";
import puisi from "../../image/puisi-vektor.png";
import hadroh from "../../image/hadroh.png";
import hadiah_cerpen from "../../image/cerpen-hadiah.png";
import Carousel from "../kompetisi/lomba-carousel"; 

export default function cerpen() {

    const lombaList = [
        {
            foto: logo_cerpen,
            kategori: 'Cerpen'
        },
        {
            foto: gambar,
            kategori: 'Menggambar'
        },
        {
            foto: esai,
            kategori: 'Esai'
        },
        {
            foto: foto,
            kategori: 'Fotografi'
        },
        {
            foto: poster,
            kategori: 'Poster'
        },
        {
            foto: puisi,
            kategori: 'Cipta Puisi'
        },
      ];

    return (
        <div>
            <Navbar
                kompetisi = "active"
            />
            <div className="container-fluid">
                <div className="row justify-content-center" id="cont-banner">
                    <div className="col-12" id="banner">

                        <div className="row mt-5 d-flex justify-content-center">
                            <div>
                                <img src={logo_cerpen} className="img-fluid float-right"></img>
                            </div>
                            <div className="mx-5 my-3">
                                <div className="row text-center">
                                    <div className="col-12">
                                        <h6 className="text-white">Kompetisi Online UIIBF 2020</h6>
                                        <h1 className="text-white">Cerpen</h1>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row justify-content-center">
                            <div className="col-8">
                                <p className="text-center text-white" id="deskripsi-banner">
                                        Cerita pendek adalah sebuah karya naratif yang 
                                    cenderung fiktif dan padat pada penyampaian 
                                    isi cerita. UIIBF menyelenggarakan kompetisi 
                                    cerita pendek dengan harapan mampu berdakwah 
                                    melalui kisah-kisah yang inspiratif dan 
                                    bernilai islam
                                </p>
                            </div>
                        </div>
                        <div className="row justify-content-center mt-3">
                            <div className="col-4 text-center">
                                <a className="btn btn-block btn-success text-white">Daftar</a>
                            </div>
                            {/* <div className="col-6"> */}
                            {/* </div> */}
                        </div>
                        <div className="row justify-content-center mt-3 mb-5">
                            <div className="col-4 text-center">
                                <a className="btn btn-block btn-warning">Kontak Kami</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row mt-5 mb-2 justify-content-center" id="cont-timeline">
                    <div className="col-10">
                        <h3>Timeline</h3>
                        <ul>
                            <li>Pendaftaran : 23 Oktober 2020 - 20 November 2020</li>
                            <li>Batas pengumpulan karya : 4 Desember 2020</li>
                            <li>
                                Periode voting juara favorit: 17 Desember 2020 
                                - 24 Desember 2020 (pukul 10.00)
                            </li>
                            <li>Pengumuman : 27 Desember 2020</li>
                        </ul>
                    </div>
                </div>

                <div className="row my-2 justify-content-center text-justify" id="cont-syaratketentuan">
                    <div className="col-10">
                        <h3>Syarat & Ketentuan</h3>
                        <ol>
                            <li>Karya belum pernah dipublikasikan di media manapun, serta belum pernah diikutsertakan pada lomba manapun</li>
                            <li>Panjang naskah cerita adalah 1000 s/d 3000 kata</li>
                            <li>Naskah diketik di dalam Ms.word dengan ukuran halaman A4, font Times New Roman ukuran 12, spasi 1,5</li>
                            <li>Peserta wajib mencantumkan lembar pernyataan orisinalitas pada saat yang bersamaan dengan pengiriman karya</li>
                            <li>Karya dilengkapi dengan biodata singkat dari penulis pada akhir tulisan yang berisikan nama, instansi/sekolah/universitas, nomor telepon, dan email</li>
                            <li>Karya yang telah dikirimkan akan menjadi hak milik UIIBF</li>
                            <li>Naskah dikirimkan dalam bentuk PDF dengan format judul file “Cerpen_(nama peserta)_(judul cerpen)”</li>
                            <li>Naskah dikirimkan ke email: emailkompelupagengs dengan format judul email “Pengiriman karya_cerpen_(nama peserta)_(judul cerpen)”</li>
                            <li>Tema “Buku Sebagai Kunci Perdamaian Dunia”</li>
                        </ol>
                    </div>
                </div>

                <div className="row my-2 justify-content-center text-justify" id="cont-sistematika">
                    <div className="col-10">
                        <h3>Sistematika Penulisan</h3>
                        <ol>
                            <li>Naskah diketik dengan jenis font Times New Roman (TNR) dengan ukuran font 12 disertai dengan spasi 1.5 cm di kertas berukuran A4.</li>
                            <li>Naskah diketik dalam bentuk rata kanan-kiri (justify).</li>
                            <li>Judul cerpen diletakkan di tengah dengan jenis font Times New Roman berukuran 14 point.</li>
                            <li>Identitas penulis dituliskan pada bagian bawah judul dengan format: Nama, Instansi.</li>
                        </ol>
                    </div>
                </div>

                <div className="row mt-2 mb-5 justify-content-center text-justify" id="cont-kriteria">
                    <div className="col-10">
                        <h3>Kriteria Penilaian</h3>
                        <ol>
                            <li>Kesesuaian isi cerita dengan tema</li>
                            <li>Pesan yang disampaikan dalam cerita</li>
                            <li>Konsistensi alur cerita </li>
                            <li>Diksi dan gaya bahasa</li>
                            <li>Kesegaran ide cerita </li>
                        </ol>
                    </div>
                </div>

                <div className="row py-5 d-flex justify-content-center bg-light">
                    <div className="mx-3">
                        <div className="row">
                            <div className="col-12" id="hadiah">
                                <h5 className="font-weight-bold">Hadiah Kompetisi</h5>
                                <p>Pemenang akan mendapatkan</p>
                                <ul>
                                    <li>Uang Pembinaan</li>
                                    <li>E-Sertifikat</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="mx-5">
                        <img src={hadiah_cerpen} className="img-fluid float-right"></img>
                    </div>
                </div>

                <div className="row py-5 d-flex justify-content-center">
                    <div className="mx-3">
                        <div className="row">
                            <div className="col-12" id="yukmenang">
                                <h4 className="font-weight-bold">Tertarik untuk jadi Pemenang?</h4>
                                <h4>Segera daftarkan dirimu sekarang!</h4>
                            </div>
                        </div>
                    </div>
                    <div className="mx-5 row d-flex align-items-center">
                        <div className="col-12">
                            <a className="mx-2 btn btn-success">Daftar</a>
                            <a className="mx-2 btn btn-outline-dark">Kontak Kami</a>
                        </div>
                    </div>
                </div>

                <div className="row px-3 py-5 bg-light">
                    <div className="col-12">
                        <div className="row">
                            <div className="col-12">
                                <h4 className="font-weight-bold text-center">Lomba Lainnya</h4>
                            </div>
                        </div>
                        <div className="slide">
                            <div className="row">
                                {lombaList.map((lombaItem) => {
                                    return (
                                        <Carousel
                                            foto = {lombaItem.foto}
                                            kategori = {lombaItem.kategori}
                                        />
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}