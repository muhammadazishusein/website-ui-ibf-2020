import React from 'react';
// import { Link } from "react-router-dom";
import "./in-story.css";
import Navbar from '../navbar/navbar';
import logo_cerpen from "../../image/cerpen-vektor.png";
import gambar from "../../image/gambar-vektor.png";
import esai from "../../image/esai-vektor.png";
import foto from "../../image/foto-vektor.png";
import poster from "../../image/poster-vektor.png";
import puisi from "../../image/puisi-vektor.png";
import mic from "../../image/microphone-vektor.png";
// import hadroh from "../../image/hadroh.png";
// import hadiah_cerpen from "../../image/cerpen-hadiah.png";
import Carousel from "../kompetisi/lomba-carousel"; 

export default function inWords() {

    const lombaList = [
        {
            foto: gambar,
            kategori: 'Drawing Academy',
            link:"drawing-academy"
        },
        {
            foto: foto,
            kategori: 'Photo Contest',
            link:"photo-contest"
        },
        {
            foto: poster,
            kategori: 'Creative Design',
            link:"creative-design"
        },
        {
            foto: puisi,
            kategori: 'In Words',
            link:"in-words"
        },
        // {
        //     foto: logo_cerpen,
        //     kategori: 'In Story',
        //     link:"in-story"
        // },
        {
            foto: esai,
            kategori: 'Writing Academy',
            link:"writing-academy"
        },
        {
            foto: mic,
            kategori: 'Voice',
            link:"voice"
        },
      ];

    return (
        <div>
            <Navbar
                kompetisi = "active"
            />
            <div className="container-fluid">
                <div className="row justify-content-center" id="cont-banner">
                    <div className="col-12" id="banner">

                        <div className="row mt-5 d-flex justify-content-center">
                            <div>
                                <img alt="logo" src={logo_cerpen} className="img-fluid float-right"></img>
                            </div>
                            <div className="mx-5 my-3">
                                <div className="row text-center">
                                    <div className="col-12">
                                        <h6 className="text-white">Kompetisi Online UIIBF 2020</h6>
                                        <h1 className="text-white">UI IBF In Story</h1>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row justify-content-center">
                            <div className="col-8">
                                <p className="text-center text-white" id="deskripsi-banner">
                                Cerita pendek adalah sebuah karya naratif yang 
                                cenderung fiktif dan padat pada penyampaian isi 
                                cerita. UI IBF menyelenggarakan kompetisi cerita 
                                pendek dengan harapan mampu berdakwah melalui 
                                kisah-kisah yang inspiratif dan bernilai islam
                                </p>
                            </div>
                        </div>
                        <div className="row justify-content-center mt-3 mb-5">
                            <div className="col-4 text-center">
                                <a href="/kompetisi/in-story/registrasi" className="btn btn-block btn-success text-white">Daftar</a>
                            </div>
                            {/* <div className="col-6"> */}
                            {/* </div> */}
                        </div>
                        {/* <div className="row justify-content-center mt-3 mb-5">
                            <div className="col-4 text-center">
                                <a href="/todo" className="btn btn-block btn-warning">Kontak Kami</a>
                            </div>
                        </div> */}
                    </div>
                </div>

                <div className="row mt-5 mb-2 justify-content-center" id="cont-timeline">
                    <div className="col-10">
                        <h3>Timeline</h3>
                        <ul>
                            <li>Pendaftaran : 15 November 2020 – 28 November 2020</li>
                            <li>Batas pengumpulan karya : 28 November 2020</li>
                            <li>Pengumuman : 27 Desember 2020 (saat acara puncak 6th UIIBF)</li>
                        </ul>
                    </div>
                </div>

                <div className="row my-2 justify-content-center text-justify" id="cont-syaratketentuan">
                    <div className="col-10">
                        <h3>Tema</h3>
                        <ul>
                            <li>“Buku Sebagai Kunci Perdamaian Dunia”</li>
                        </ul>
                    </div>
                </div>

                <div className="row my-2 justify-content-center text-justify" id="cont-syaratketentuan">
                    <div className="col-10">
                        <h3>Syarat & Ketentuan</h3>
                        <ol>
                            <li>Karya belum pernah dipublikasikan di media manapun, serta belum pernah diikutsertakan pada lomba manapun.</li>
                            <li>Panjang naskah cerita adalah 1000 s/d 3000 kata.</li>
                            <li>Naskah diketik di dalam Ms.word dengan ukuran halaman A4, font Times New Roman ukuran 12, spasi 1,5.</li>
                            <li>Peserta wajib mencantumkan lembar pernyataan orisinalitas pada saat yang bersamaan dengan pengiriman karya.</li>
                            <li>Karya dilengkapi dengan biodata singkat dari penulis pada akhir tulisan yang berisikan nama, instansi/sekolah/universitas, nomor telepon, dan email.</li>
                            <li>Karya yang telah dikirimkan akan menjadi hak milik UIIBF.</li>
                            <li>Naskah dikirimkan dalam bentuk PDF dengan format judul file “Cerpen_(nama peserta)_(judul cerpen)”.</li>
                            <li>Naskah dikirimkan ke email: kompetisiuiibf@gmail.com dengan format judul email “Pengiriman karya_cerpen_(nama peserta)_(judul cerpen)”.</li>
                        </ol>
                    </div>
                </div>

                <div className="row my-2 justify-content-center text-justify" id="cont-syaratketentuan">
                    <div className="col-10">
                        <h3>Sistematika Penulisan</h3>
                        <ol>
                            <li>Naskah diketik dengan jenis font Times New Roman (TNR) dengan ukuran font 12 disertai dengan spasi 1.5 cm di kertas berukuran A4.</li>
                            <li>Naskah diketik dalam bentuk rata kanan-kiri (justify).</li>
                            <li>Judul cerpen diletakkan di tengah dengan jenis font Times New Roman berukuran 14 point.</li>
                            <li>Identitas penulis dituliskan pada bagian bawah judul dengan format: Nama, Instansi.</li>
                        </ol>
                    </div>
                </div>

                <div className="row mt-2 mb-5 justify-content-center text-justify" id="cont-kriteria">
                    <div className="col-10">
                        <h3>Kriteria Penilaian</h3>
                        <ul>
                            <li>Kesesuaian dengan tema (10%)</li>
                            <li>Pesan yang disampaikan dalam cerita (25%)</li>
                            <li>Konsistensi alur cerita (25%)</li>
                            <li>Diksi dan gaya bahasa (20%)</li>
                            <li>Kesegaran ide cerita (20%)</li>
                        </ul>
                    </div>
                </div>

                <div className="row py-5 d-flex justify-content-center bg-light">
                    <div className="mx-3">
                        <div className="row align-items-center">
                            <div className="col-12">
                                <h3 className="font-weight-bold text-center">Hadiah Kompetisi</h3>
                                <div className="row d-flex justify-content-center">
                                    <div className="ml-3 mr-3">
                                        <p>Pemenang akan mendapatkan</p>
                                        <ul>
                                            <li>Uang Pembinaan</li>
                                            <li>E-Sertifikat</li>
                                        </ul>
                                    </div>
                                    <div className="">
                                        <p>Peserta akan mendapatkan</p>
                                        <ul>
                                            <li>E-Sertifikat peserta</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <div className="mx-5">
                        <img alt="hadiah cerpen" src={hadiah_cerpen} className="img-fluid float-right"></img>
                    </div> */}
                </div>

                <div className="row py-5 d-flex justify-content-center">
                    <div className="mx-3">
                        <div className="row">
                            <div className="col-12" id="yukmenang">
                                <h4 className="font-weight-bold">Tertarik untuk jadi Pemenang?</h4>
                                <h4>Segera daftarkan dirimu sekarang!</h4>
                            </div>
                        </div>
                    </div>
                    <div className="mx-5 row d-flex align-items-center">
                        <div className="col-12">
                            <a href="/kompetisi/in-story/registrasi" className="mx-2 btn btn-success">Daftar</a>
                            {/* <a href="/todo" className="mx-2 btn btn-outline-dark">Kontak Kami</a> */}
                        </div>
                    </div>
                </div>

                <div className="row px-5 py-5">
                    <div className="col-12">
                        <div className="row mb-3">
                            <div className="col-12">
                                <h4 className="font-weight-bold text-center">Lomba Lainnya</h4>
                            </div>
                        </div>
                        <div className="slide">
                            <div hidden>
                                Icons made by 
                                <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a>
                                from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
                            </div>
                            <div className="row align-items-center">
                                {lombaList.map((lombaItem) => {
                                    return (
                                        <Carousel
                                            foto = {lombaItem.foto}
                                            kategori = {lombaItem.kategori}
                                            abu = {false}
                                            link = {lombaItem.link}
                                        />
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}