import React, { useState }  from 'react';
import axiosInstance from '../axiosApi';
import Navbar from '../navbar/navbar';
import Proses from "./prosesregis";
import logo_bni from "../../image/bni.png";
import "./registrasi.css";

export default function Registrasi(props) {
    const [phone, setPhone] = useState('');
    const [hasPhoneError, setHasPhoneError] = useState(false);
    const [phoneEmpty, setPhoneEmpty] = useState(false);
    const [notNumb, setNotNumb] = useState(false);
    const [notPlus, setNotPlus] = useState(false)

    function handlePhoneChange(event) {
        const inputValue = event.target.value;
        const isEmpty = inputValue === '';
        const notPhone = inputValue.length < 11 || inputValue.length > 16;
        var plusCek = true;
        var cekNum = true;

        if(inputValue.length === 1) {
            plusCek = inputValue === '+';
        }
        else {
            cekNum = /^-?[\d.]+(?:e-?\d+)?$/.test(inputValue.substring(1));

            if(cekNum) {
                plusCek = inputValue.substring(0, 1) === '+';
            }
        }

        setPhone(inputValue);
        setHasPhoneError(notPhone);
        setPhoneEmpty(isEmpty);
        setNotNumb(!cekNum);
        setNotPlus(!plusCek);
    }

    let phoneErrorText;

    if(phoneEmpty) {
        phoneErrorText = (
            <div className="error-message">
                <p className='text-danger'>Kamu harus memasukkan nomor telepon.</p>
            </div>
        );
    }
    
    else if (notNumb) {
        phoneErrorText = (
            <div className="error-message">
                <p className='text-danger'>Hanya bisa diisi angka.</p>
            </div>
        );
    }
    
    else if (notPlus) {
        phoneErrorText = (
            <div className="error-message">
                <p className='text-danger'>Karakter pertama harus tanda +</p>
            </div>
        );
    }

    else if(hasPhoneError) {
        phoneErrorText = (
            <div className="error-message">
                <p className="text-warning">Nomor telepon harus diisi 9 hingga 14 digit.</p>
            </div>
        );
    }

    const [email, setEmail] = useState('');
    const [hasEmailError, setHasEmailError] = useState(false);
    const [emptyEmail, setEmptyEmail] = useState(false)

    function handleEmailChange(event) {
        const inputValue = event.target.value;
        const isEmpty = inputValue === '';
        const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        const isError = regex.test(String(inputValue));

        setEmail(inputValue);
        setHasEmailError(!isError);
        setEmptyEmail(isEmpty);
    }

    let emailErrorText;

    if(emptyEmail) {
        emailErrorText = (
            <div className="error-message">
                <p className='text-danger'>Kamu harus memasukkan e-mail.</p>
            </div>
        );
    }

    else if(hasEmailError) {
        emailErrorText = (
            <div className="error-message">
                <p className="text-warning">Email yang dimasukkan belum valid.</p>
            </div>
        );
    }

    const [name, setName] = useState('');
    const [nameError, setNameError] = useState(false);

    function handleName(event) {
        const inputValue = event.target.value;
        const isEmpty = inputValue === '';

        setName(inputValue);
        setNameError(isEmpty);
    }

    let nameErrMsg;

    if(nameError) {
        nameErrMsg = (
            <div className="error-message">
                <p className="text-danger">Kamu harus mengisi nama.</p>
            </div>
        );
    }

    const [dom, setDom] = useState('');
    const [domError, setDomError] = useState(false);

    function handleDom(event) {
        const inputValue = event.target.value;
        const isEmpty = inputValue === '';

        setDom(inputValue);
        setDomError(isEmpty);
    }

    let domErrMsg;

    if(domError) {
        domErrMsg = (
            <div className="error-message">
                <p className="text-danger">Kamu harus mengisi domisili.</p>
            </div>
        );
    }

    const [ins, setIns] = useState('');
    const [insError, setInsError] = useState(false);

    function handleIns(event) {
        const inputValue = event.target.value;
        const isEmpty = inputValue === '';

        setIns(inputValue);
        setInsError(isEmpty);
    }

    let insErrMsg;

    if(insError) {
        insErrMsg = (
            <div className="error-message">
                <p className="text-danger">Kamu harus mengisi institusi.</p>
            </div>
        );
    }

    const [harga, setHarga] = useState("0");

    // let successSubmit;

    const handleSubmit = (event) => {
        event.preventDefault();
        console.log("Masuk")
        document.getElementById("regis").style.display = "none";
        document.getElementById("checkout").style.display = "";
        // if(props.kategori === "Lomba" || props.lomba === "Self Development" || props.lomba === "Inovasi Teknologi"){
        //     console.log("Masuk")
        //     document.getElementById("regis").style.display = "none";
        //     document.getElementById("checkout").style.display = "";
        // } else {
        //     var bayar;
        //     if(props.lomba === "Workshop-1" || props.lomba === "Workshop-2") {
        //         bayar = 15000;
        //     }
            
        //     else {
        //         bayar = 0;
        //     }

        //     axiosInstance.post("/registrasi/", {
        //         'nominal_bayar': props.harga,
        //         'registrasi_user': email,
        //         'nama':props.lomba,
        //         'tipe':props.kategori,
        //         'user':{
        //             "email":email,
        //             "domisili":dom,
        //             "institusi_pendidikan":ins,
        //             "phone_number":phone,
        //             "nama_lengkap":name
        //         }
        //     }).then(function (response) {
        //         if(props.lomba === "Workshop-1" || props.lomba === "Workshop-2") {
        //             setHarga(response.data.registrasi);
        //             document.getElementById("regis").style.display = "none";
        //             document.getElementById("ringkasan").style.display = "";   
        //         }
        //         else {
        //             window.alert("Pendaftaran berhasil dilakukan. Terimakasih!")
        //             window.location.href = "http://www.uiibf.com/acara";
        //         }
        //     }).catch((error) => {
        //         document.getElementById("warning-harga").textContent = "Gagal membuat checkout. Silahkan coba lagi setelah beberapa saat.";
        //     }) 
        // }
    }
    
    const balikRegis = (event) => {
        event.preventDefault();
        document.getElementById("regis").style.display = "";
        document.getElementById("checkout").style.display = "none";
    }

    function handleHarga(event) {
        setHarga(event.target.value);
    }
    
    const handleKonfirm = (event) => {
        event.preventDefault();
        console.log(harga);
        document.getElementById("warning-harga").textContent = "";
        // if (harga === "0" && props.kategori !== "Acara") {
        //     document.getElementById("warning-harga").textContent = "Kamu belum memilih infaq terbaik :(";
        // } else {
        axiosInstance.post("/registrasi/", {
            'nominal_bayar': harga,
            'registrasi_user': email,
            'nama':props.lomba,
            'tipe':props.kategori,
            'user':{
                "email":email,
                "domisili":dom,
                "institusi_pendidikan":ins,
                "phone_number":phone,
                "nama_lengkap":name
            }
        }).then(function (response) {
            if (harga === "0") {
                window.alert("Pendaftaran berhasil dilakukan. Terimakasih!")
                window.location.href = "http://www.uiibf.com/acara";
                return
            }
            setHarga(response.data.registrasi);
            document.getElementById("checkout").style.display = "none";
            document.getElementById("ringkasan").style.display = "";   

        }).catch((error) => {
            document.getElementById("warning-harga").textContent = "Gagal membuat checkout. Silahkan coba lagi setelah beberapa saat.";
        })     
        // }      
    }

    let navbar;
    let buttonBatal;
    let infaqAcara;
    // let zeroOpt;

    if(props.kategori === "Lomba") {
        navbar = (
            <Navbar
                kompetisi = "active"
            />
        );

        buttonBatal = (
            <a href={"/kompetisi/" + props.link} className="btn btn-outline-danger">Batalkan</a>
        );
    }
    else if(props.kategori === "Acara") {
        navbar = (
            <Navbar
                acara = "active"
            />
        );

        buttonBatal = (
            <a href={"/acara/" + props.link} className="btn btn-outline-danger">Batalkan</a>
        );

        // if (props.lomba !== "Bedah Buku"){
        //     zeroOpt = (
        //         <div>
        //             <input type="radio" name="harga" className="radio" id="radio7" onChange={handleHarga} value="0"/>
        //             <label htmlFor="radio7" className="btn labelRadio"><h6>Rp0</h6></label>
        //         </div>
        //     )
        // }
        
        if (props.lomba === "Self Development"){
            infaqAcara =(
                <p className="mt-3">Infaq dari anda akan diinfaqan seluruhnya ke Zakat Sukses untuk pembangunan rumah tahfidz</p>
            );
        }
        else if (props.lomba === "Inovasi Teknologi"){
            infaqAcara =(
                <p className="mt-3">Infaq dari anda akan diinfaqan seluruhnya ke ACT untuk pengadaan alat belajar daring bagi pelajar di kota depok</p>
            );
        }
    }

    return (
        <div className="tampungan">
            {navbar}
            <div id="regis">
                <Proses
                    registrasi = "process"
                />

                <div className="container">
                    <h3>Registrasi {props.kategori}</h3>
                    <h5>{props.lomba}</h5>

                    <form onSubmit={handleSubmit} className="form-row form-lomba">
                        <input type="hidden" value={props.lomba}/>

                        <label htmlFor="nama">Nama Lengkap</label>
                        <input required placeholder="Nama Lengkap" type="text" name="nama" className="form-control" onChange={(event) => {handleName(event)}} value={name}/>

                        {nameErrMsg}
                        
                        <label htmlFor="nomor">Nomor HP / WhatsApp</label>
                        <input required placeholder="+6288888888888" type="tel" name="nomor" className="form-control" pattern="^\+62?\d{9,15}" onChange={(event) => {handlePhoneChange(event)}} value={phone}/>

                        {phoneErrorText}
                        
                        <label htmlFor="email">E-Mail Aktif</label>
                        <input required placeholder="@mail.com" type="email" name="email" className="form-control" onChange={(event) => {handleEmailChange(event)}} value={email}/>

                        {emailErrorText}
                        
                        <label htmlFor="domisili">Domisili</label>
                        <input required placeholder="Domisili" type="text" name="domisili" className="form-control" onChange={(event) => {handleDom(event)}} value={dom}/>

                        {domErrMsg}
                        
                        <label htmlFor="institusi">Institusi</label>
                        <input required placeholder="Institusi" type="text" name="institusi" className="form-control" onChange={(event) => {handleIns(event)}} value={ins}/>

                        {insErrMsg}

                        <label htmlFor="follow">Sudah Follow <a className="text-info" href="https://www.instagram.com/uiibf/">Instagram</a> UI IBF?</label>
                        <div className="form-check cek-follow mb-3">
                            <input required type="checkbox" name="follow" id="follow" className="form-check-input"/>
                            <label htmlFor="labelFollow" className="form-check-label">Ya, Sudah</label>
                        </div>

                        <label htmlFor="subscribe" style={{marginTop: "2vh"}}>Sudah Follow <a href="https://instagram.com/zakatsukses?igshid=rkfltc8npz29" target="_blank" rel="noopener noreferrer">Instagram </a> dan Subscribe <a href="https://youtu.be/9YMkYTEOvn4" target="_blank" rel="noopener noreferrer">Youtube</a> Zakat Sukses?</label>
                        <div className="form-check cek-follow">
                            <input required type="checkbox" name="subscribe" id="subscribe" className="form-check-input"/>
                            <label htmlFor="labelFollow" className="form-check-label">Ya, Sudah</label>
                        </div>
                        
                        {buttonBatal}
                        <button type="submit" className="btn btn-next ml-auto">Selanjutnya</button>
                    </form>

                    <p id="warning-harga" className="text-danger mb-4" style={{marginBottom: 0}}></p>
                </div>
            </div>

            <div id="checkout" style={{display: "none"}}>
                <Proses
                    checkout = "process"
                />

                <div className="container">
                    <h3>Checkout</h3>
                    <h5>{props.lomba}</h5>

                    <p>Silakan pilih nominal yang ingin dibayarkan sebagai bentuk infak terbaik untuk agenda syiar terbesar di UI.</p>

                    <form onSubmit={handleKonfirm} className="form-row">
                        <div className="card card-checkout">
                            <div className="card-body mb-2">
                                <h4>Jumlah nominal infak yang disediakan</h4>

                                <p>Harap pilih salah satu dari nominal di bawah ini</p>

                                <div className="radio-harga">
                                    <div>
                                        <input type="radio" name="harga" className="radio" id="radio7" onChange={handleHarga} value="0"/>
                                        <label htmlFor="radio7" className="btn labelRadio"><h6>Rp0</h6></label>
                                    </div>
                                    <div>
                                        <input type="radio" name="harga" className="radio" id="radio1" onChange={handleHarga} value="10000"/>
                                        <label htmlFor="radio1" className="btn labelRadio"><h6>Rp10.000</h6></label>
                                    </div>
                                    
                                    <div>
                                        <input type="radio" name="harga" className="radio" id="radio2" onChange={handleHarga} value="20000"/>
                                        <label htmlFor="radio2" className="btn labelRadio"><h6>Rp20.000</h6></label>
                                    </div>

                                    <div>
                                        <input type="radio" name="harga" className="radio" id="radio3" onChange={handleHarga} value="30000"/>
                                        <label htmlFor="radio3" className="btn labelRadio"><h6>Rp30.000</h6></label>
                                    </div>

                                    <div>
                                        <input type="radio" name="harga" className="radio" id="radio4" onChange={handleHarga} value="50000"/>
                                        <label htmlFor="radio4" className="btn labelRadio"><h6>Rp50.000</h6></label>
                                    </div>

                                    <div>
                                        <input type="radio" name="harga" className="radio" id="radio5" onChange={handleHarga} value="100000"/>
                                        <label htmlFor="radio5" className="btn labelRadio"><h6>Rp100.000</h6></label>
                                    </div>

                                    <div>
                                        <input type="radio" name="harga" className="radio" id="radio6" onChange={handleHarga} value="200000"/>
                                        <label htmlFor="radio6" className="btn labelRadio"><h6>Rp200.000</h6></label>
                                    </div>
                                </div>
                                
                                <p id="warning-harga" className="text-danger" style={{marginBottom: 0}}></p>

                                {infaqAcara}
                            </div>
                        </div>
                        <div className="container">
                            <div className="d-flex justify-content-end">
                                {/* <div className="col-12"> */}
                                    <p id="warning-harga" className="text-danger mb-4" style={{marginBottom: 0}}></p>
                                {/* </div> */}
                            </div>
                            <div className="d-flex justify-content-between">
                                {/* <div className="col-12"> */}
                                    <button type="button" onClick={balikRegis} className="btn btn-outline-danger">Sebelumnya</button>
                                    <button type="submit" className="btn btn-success ml-auto">Konfirmasi</button>
                                {/* </div> */}
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div id="ringkasan" style={{display: "none"}}>
                <Proses
                    ringkasan = "process"
                />

                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <h3>Ringkasan ✓</h3>
                            <p>Alhamdulillah, satu langkah lagi registrasimu berhasil! 
                                Silakan transfer donasi sebelum periode pendaftaran 
                                berakhir dengan rincian sebagai berikut. Kami tunggu ya! 
                            </p>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <div className="card">
                                <div className="card-body">

                                    <h5>Transfer Pembayaran ke Nomor Rekening</h5>
                                    <br></br>
                                    <div className="row d-flex align-items-center">
                                        <div className="mr-4">
                                            <img alt="Logo BNI" src={logo_bni}></img>
                                        </div>
                                        <div className="mt-4 ml-3">
                                            <h5>BNI</h5>
                                            <h5>Nomor Rekening: 721744815</h5>
                                            <h5>a.n Ahmad Ihsan Rizkinda</h5>
                                        </div>
                                    </div>

                                    <button className="btn btn-outline-info"
                                        onClick={() => {
                                            navigator.clipboard.writeText("721744815");
                                            document.getElementById("sukses-norek").textContent = "Berhasil di copy!";
                                        }}>
                                        Salin No. Rek
                                    </button>
                                    <br></br>
                                    <p id="sukses-norek" className="text-success mt-3"></p>
                                    
                                    <div className="mt-5">
                                        <h5>Jumlah yang harus dibayar</h5>
                                        <h5><b>Rp {harga}</b></h5>
                                    </div>
                                    <button className="btn btn-outline-info"
                                        onClick={() => {
                                            navigator.clipboard.writeText({harga});
                                            document.getElementById("sukses-jumlah").textContent = "Berhasil di copy!";
                                        }}>
                                        Salin Jumlah
                                    </button>
                                    <br></br>
                                    <p id="sukses-jumlah" className="text-success mt-3"></p>

                                    <div className="mt-5 mb-2">
                                        <p><i>Catatan Khusus</i></p>
                                        <p className="mb-0"><i>*anda akan mendapat email yang berisi ringkasan dari pendaftaran yang telah anda lakukan.</i></p>
                                        <p className="mb-0"><i>*setelah melakukan pembayaran, anda akan mendapat email konfirmasi bahwa pendaftaran {props.kategori} berhasil.</i></p>
                                        <p className="mb-0"><i>*jika tidak mendapat email konfirmasi, mohon hubungi CP: Iman Herlana - 082297614941</i></p>
                                    </div>

                                </div>
                            </div>
                            <div className="row mt-3">
                                <div className="col-12">
                                    <a href="/kompetisi" className="btn btn-success text-white">Kembali ke Beranda</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}