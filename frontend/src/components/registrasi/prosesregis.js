import React from 'react';
import "./registrasi.css";

export default function prosesregis(props) {
    return (
        <div className="progressbar">
            <ul className="px-0">
                <li className={props.registrasi}>Registrasi</li>
                <li className={props.checkout}>Checkout</li>
                <li className={props.ringkasan}>Ringkasan</li>
            </ul>
            <hr className=""></hr>
        </div>
    )
}