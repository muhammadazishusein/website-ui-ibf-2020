function Radio(props){
    const [selected, setSelected] = useState(false)
    
    function toggle() {
        const {onChange} = this.context.radioGroup;
        setSelected(!selected);
    }

    let classname = this.state.selected ? 'activeRadio' : ''
    return (
      <button type="button" className={"btn labelRadio " + classname} onClick={ toggle } value={ props.value }>
          { props.value}
      </button>
    )
}

export default Radio;