import React from 'react';
// import { Link } from "react-router-dom";
import "./in-words.css";
import Navbar from '../navbar/navbar';
import logo_cerpen from "../../image/cerpen-vektor.png";
import gambar from "../../image/gambar-vektor.png";
import esai from "../../image/esai-vektor.png";
import foto from "../../image/foto-vektor.png";
import poster from "../../image/poster-vektor.png";
import puisi from "../../image/puisi-vektor.png";
import mic from "../../image/microphone-vektor.png";
// import hadroh from "../../image/hadroh.png";
// import hadiah_cerpen from "../../image/cerpen-hadiah.png";
import Carousel from "../kompetisi/lomba-carousel"; 

export default function inWords() {

    const lombaList = [
        {
            foto: gambar,
            kategori: 'Drawing Academy',
            link:"drawing-academy"
        },
        {
            foto: foto,
            kategori: 'Photo Contest',
            link:"photo-contest"
        },
        {
            foto: poster,
            kategori: 'Creative Design',
            link:"creative-design"
        },
        // {
        //     foto: puisi,
        //     kategori: 'In Words',
        //     link:"in-words"
        // },
        {
            foto: logo_cerpen,
            kategori: 'In Story',
            link:"in-story"
        },
        {
            foto: esai,
            kategori: 'Writing Academy',
            link:"writing-academy"
        },
        {
            foto: mic,
            kategori: 'Voice',
            link:"voice"
        },
      ];

    return (
        <div>
            <Navbar
                kompetisi = "active"
            />
            <div className="container-fluid">
                <div className="row justify-content-center" id="cont-banner">
                    <div className="col-12" id="banner">

                        <div className="row mt-5 d-flex justify-content-center">
                            <div>
                                <img alt="logo" src={puisi} className="img-fluid float-right"></img>
                            </div>
                            <div className="mx-5 my-3">
                                <div className="row text-center">
                                    <div className="col-12">
                                        <h6 className="text-white">Kompetisi Online UIIBF 2020</h6>
                                        <h1 className="text-white">UI IBF In Words</h1>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row justify-content-center">
                            <div className="col-8">
                                <p className="text-center text-white" id="deskripsi-banner">
                                Puisi merupakan salah satu bentuk karya sastra 
                                yang diungkapkan dengan bantuan kata-kata kiasan. 
                                Puisi merupakan hasil ungkapan perasaan yang 
                                dituliskan penulis dengan untaian gaya bahasa 
                                yang sesuai dengan ciri penulis.
                                </p>
                            </div>
                        </div>
                        <div className="row justify-content-center mt-3 mb-5">
                            <div className="col-4 text-center">
                                <a href="/kompetisi/in-words/registrasi" className="btn btn-block btn-success text-white">Daftar</a>
                            </div>
                            {/* <div className="col-6"> */}
                            {/* </div> */}
                        </div>
                        {/* <div className="row justify-content-center mt-3 mb-5">
                            <div className="col-4 text-center">
                                <a href="/todo" className="btn btn-block btn-warning">Kontak Kami</a>
                            </div>
                        </div> */}
                    </div>
                </div>

                <div className="row mt-5 mb-2 justify-content-center" id="cont-timeline">
                    <div className="col-10">
                        <h3>Timeline</h3>
                        <ul>
                            <li>Pendaftaran : 15 November 2020 – 28 November 2020</li>
                            <li>Batas pengumpulan karya : 28 November 2020</li>
                            <li>Pengumuman : 27 Desember 2020 (saat acara puncak 6th UIIBF)</li>
                        </ul>
                    </div>
                </div>

                <div className="row my-2 justify-content-center text-justify" id="cont-syaratketentuan">
                    <div className="col-10">
                        <h3>Tema</h3>
                        <ul>
                            <li>“Tingkatkan literasi, wujudkan generasi cerdas”</li>
                            <li>"Melihat dunia melalui buku"</li>
                            <li>"Para superhero di masa pandemi"</li>
                            <li>"Mengubah dunia melalui tulisan"</li>
                        </ul>
                    </div>
                </div>

                <div className="row my-2 justify-content-center text-justify" id="cont-syaratketentuan">
                    <div className="col-10">
                        <h3>Syarat & Ketentuan</h3>
                        <ol>
                            <li>Naskah yang dikirimkan adalah karya asli dan belum pernah dipublikasikan serta. belum pernah diikutsertakan dalam perlombaan manapun.</li>
                            <li>Naskah berbahasa Indonesia.</li>
                            <li>Naskah yang ditulis maksimal 5 halaman</li>
                            <li>Judul puisi bebas sesuai dengan tema yang telah ditentukan.</li>
                            <li>Naskah yang telah dikirimkan menjadi hak panitia sepenuhnya dan dapat dipublikasikan dengan tetap mencantumkan identitas penulis.</li>
                            <li>Naskah dikirimkan dalam bentuk PDF dengan format judul file “Puisi_(nama peserta)_(judul puisi)”</li>
                            <li>Naskah dikirimkan ke email: kompetisiuiibf@gmail.com dengan format judul email “Pengiriman karya_Puisi_(nama peserta)_(judul puisi)”</li>
                            <li>Hasil karya diupload di instagram masing-masing, wajib tag akun instagram @salam_ui dan @uiibf serta menggunakan tagar/hashtag #LombaPuisiuiibf6 #LombaPuisi #uiibf6</li>
                            <li>Akun instagram tidak di private sampai pengumuman</li>
                            <li>Jumlah like pada postingan instagram dapat memengaruhi hasil akhir penilaian.</li>
                        </ol>
                    </div>
                </div>

                <div className="row my-2 justify-content-center text-justify" id="cont-syaratketentuan">
                    <div className="col-10">
                        <h3>Sistematika Penulisan</h3>
                        <ol>
                            <li>Naskah diketik dengan jenis font Times New Roman (TNR) dengan ukuran font 12 disertai dengan spasi 1.5 cm di kertas berukuran A4.</li>
                            <li>Naskah diketik dalam bentuk rata kanan-kiri (justify).</li>
                            <li>Judul puisi diletakkan di tengah dengan jenis font Times New Roman berukuran 14 point.</li>
                            <li>Identitas penulis dituliskan pada bagian bawah judul dengan format: Nama, Instansi.</li>
                        </ol>
                    </div>
                </div>

                <div className="row mt-2 mb-5 justify-content-center text-justify" id="cont-kriteria">
                    <div className="col-10">
                        <h3>Kriteria Penilaian</h3>
                        <ul>
                            <li>Kesesuaian dengan tema (10%)</li>
                            <li>Keutuhan dan keselerasan (20%)</li>
                            <li>Diksi dan gaya bahasa (30%)</li>
                            <li>Orisinalitas (15%)</li>
                            <li>Isi atau gagasan (20%)</li>
                        </ul>
                    </div>
                </div>

                <div className="row py-5 d-flex justify-content-center bg-light">
                    <div className="mx-3">
                        <div className="row align-items-center">
                            <div className="col-12">
                                <h3 className="font-weight-bold text-center">Hadiah Kompetisi</h3>
                                <div className="row d-flex justify-content-center">
                                    <div className="ml-3 mr-3">
                                        <p>Pemenang akan mendapatkan</p>
                                        <ul>
                                            <li>Uang Pembinaan</li>
                                            <li>E-Sertifikat</li>
                                        </ul>
                                    </div>
                                    <div className="">
                                        <p>Peserta akan mendapatkan</p>
                                        <ul>
                                            <li>E-Sertifikat peserta</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <div className="mx-5">
                        <img alt="hadiah cerpen" src={hadiah_cerpen} className="img-fluid float-right"></img>
                    </div> */}
                </div>

                <div className="row py-5 d-flex justify-content-center">
                    <div className="mx-3">
                        <div className="row">
                            <div className="col-12" id="yukmenang">
                                <h4 className="font-weight-bold">Tertarik untuk jadi Pemenang?</h4>
                                <h4>Segera daftarkan dirimu sekarang!</h4>
                            </div>
                        </div>
                    </div>
                    <div className="mx-5 row d-flex align-items-center">
                        <div className="col-12">
                            <a href="/kompetisi/in-words/registrasi" className="mx-2 btn btn-success">Daftar</a>
                            {/* <a href="/todo" className="mx-2 btn btn-outline-dark">Kontak Kami</a> */}
                        </div>
                    </div>
                </div>

                <div className="row px-5 py-5">
                    <div className="col-12">
                        <div className="row mb-3">
                            <div className="col-12">
                                <h4 className="font-weight-bold text-center">Lomba Lainnya</h4>
                            </div>
                        </div>
                        <div className="slide">
                            <div hidden>
                                Icons made by 
                                <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a>
                                from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
                            </div>
                            <div className="row align-items-center">
                                {lombaList.map((lombaItem) => {
                                    return (
                                        <Carousel
                                            foto = {lombaItem.foto}
                                            kategori = {lombaItem.kategori}
                                            abu = {false}
                                            link = {lombaItem.link}
                                        />
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}