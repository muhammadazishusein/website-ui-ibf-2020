import React from 'react';
// import { Link } from "react-router-dom";
import "./voice.css";
import Navbar from '../navbar/navbar';
import logo_cerpen from "../../image/cerpen-vektor.png";
import gambar from "../../image/gambar-vektor.png";
import esai from "../../image/esai-vektor.png";
import foto from "../../image/foto-vektor.png";
import poster from "../../image/poster-vektor.png";
import puisi from "../../image/puisi-vektor.png";
import mic from "../../image/microphone-vektor.png";
// import hadroh from "../../image/hadroh.png";
// import hadiah_cerpen from "../../image/cerpen-hadiah.png";
import Carousel from "../kompetisi/lomba-carousel"; 

export default function writingAcademy() {

    const lombaList = [
        {
            foto: gambar,
            kategori: 'Drawing Academy',
            link:"drawing-academy"
        },
        {
            foto: foto,
            kategori: 'Photo Contest',
            link:"photo-contest"
        },
        {
            foto: poster,
            kategori: 'Creative Design',
            link:"creative-design"
        },
        {
            foto: puisi,
            kategori: 'In Words',
            link:"in-words"
        },
        {
            foto: logo_cerpen,
            kategori: 'In Story',
            link:"in-story"
        },
        {
            foto: esai,
            kategori: 'Writing Academy',
            link:"writing-academy"
        },
        // {
        //     foto: mic,
        //     kategori: 'Voice',
        //     link:"voice"
        // },
      ];

    return (
        <div>
            <Navbar
                kompetisi = "active"
            />
            <div className="container-fluid">
                <div className="row justify-content-center" id="cont-banner">
                    <div className="col-12" id="banner">

                        <div className="row mt-5 d-flex justify-content-center">
                            <div>
                                <img alt="logo" src={mic} className="img-fluid float-right"></img>
                            </div>
                            <div className="mx-5 my-3">
                                <div className="row text-center">
                                    <div className="col-12">
                                        <h6 className="text-white">Kompetisi Online UIIBF 2020</h6>
                                        <h1 className="text-white">UI IBF Voice</h1>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row justify-content-center">
                            <div className="col-8">
                                <p className="text-center text-white" id="deskripsi-banner">
                                UI IBF Voice merupakan salah satu lomba yang 
                                bertujuan untuk memberi wadah kepada seseorang 
                                untuk menyalurkan kreativitasnya serta menggali 
                                minat dan bakat dalam bidang bernyanyi dan 
                                bermusik bernuansa islam yang khusus untuk 
                                laki-laki.
                                </p>
                            </div>
                        </div>
                        <div className="row justify-content-center mt-3 mb-5">
                            <div className="col-4 text-center">
                                <a href="/kompetisi/voice/registrasi" className="btn btn-block btn-success text-white">Daftar</a>
                            </div>
                            {/* <div className="col-6"> */}
                            {/* </div> */}
                        </div>
                        {/* <div className="row justify-content-center mt-3 mb-5">
                            <div className="col-4 text-center">
                                <a href="/todo" className="btn btn-block btn-warning">Kontak Kami</a>
                            </div>
                        </div> */}
                    </div>
                </div>

                <div className="row mt-5 mb-2 justify-content-center" id="cont-timeline">
                    <div className="col-10">
                        <h3>Timeline</h3>
                        <ul>
                            <li>Pendaftaran : 15 November 2020 – 28 November 2020</li>
                            <li>Batas pengumpulan karya : 28 November 2020</li>
                            <li>Sesi Final: 25 Desember 2020</li>
                            <li>Pengumuman : 27 Desember 2020 (saat acara puncak 6th UIIBF)</li>
                        </ul>
                    </div>
                </div>

                <div className="row my-2 justify-content-center text-justify" id="cont-syaratketentuan">
                    <div className="col-10">
                        <h3>Ketentuan Lagu</h3>
                        <ul>
                            <li>Lagu yang dibawakan bebas dan bernuansa islam.</li>
                            <li>Tidak terdapat lirik yang bertentangan dengan nilai islam di dalam lagu.</li>
                            <li>Tidak terdapat lirik yang menjatuhkan pihak lain di dalam lagu</li>
                        </ul>
                    </div>
                </div>

                <div className="row my-2 justify-content-center text-justify" id="cont-syaratketentuan">
                    <div className="col-10">
                        <h3>Syarat & Ketentuan</h3>
                        <ol>
                            <li>Peserta merupakan laki-laki umur 12 tahun ke atas (terbuka untuk umum).</li>
                            <li>Peserta merupakan perorangan.</li>
                            <li>Peserta boleh menggunakan alat musik.</li>
                            <li>Peserta mengirimkan karya dalam format video .mp4 maksimal 4 menit tanpa editing.</li>
                            <li>Video yang dikirimkan merupakan karya asli pribadi Peserta bertanggung jawab penuh terhadap orisinalitas karya yang dikumpulkan.</li>
                            <li>Karya yang dikirimkan adalah karya yang belum pernah diikutsertakan dalam lomba lain.</li>
                            <li>Karya dikirimkan di link <a href="http://bit.ly/PengumpulanVideoUIIBFVOICE">http://bit.ly/PengumpulanVideoUIIBFVOICE</a>.</li>
                            <li>Akan dipilih 10 peserta terbaik yang akan bernyanyi live via zoom  (link akan dikirimkan menyusul).</li>
                            <li>Keputusan juri tidak dapat diganggu gugat.</li>
                        </ol>
                    </div>
                </div>

                <div className="row mt-2 mb-5 justify-content-center text-justify" id="cont-kriteria">
                    <div className="col-10">
                        <h3>Kriteria Penilaian</h3>
                        <ul>
                            <li>Kualitas Tone</li>
                            <li>Teknik</li>
                            <li>Pembawaan</li>
                            <li>Penampilan</li>
                            <li>Pesan yang disampaikan</li>
                        </ul>
                    </div>
                </div>

                <div className="row py-5 d-flex justify-content-center bg-light">
                    <div className="mx-3">
                        <div className="row align-items-center">
                            <div className="col-12">
                                <h3 className="font-weight-bold text-center">Hadiah Kompetisi</h3>
                                <div className="row d-flex justify-content-center">
                                    <div className="ml-3 mr-3">
                                        <p>Pemenang akan mendapatkan</p>
                                        <ul>
                                            <li>Uang Pembinaan</li>
                                            <li>E-Sertifikat</li>
                                        </ul>
                                    </div>
                                    <div className="">
                                        <p>Peserta akan mendapatkan</p>
                                        <ul>
                                            <li>E-Sertifikat peserta</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <div className="mx-5">
                        <img alt="hadiah cerpen" src={hadiah_cerpen} className="img-fluid float-right"></img>
                    </div> */}
                </div>

                <div className="row py-5 d-flex justify-content-center">
                    <div className="mx-3">
                        <div className="row">
                            <div className="col-12" id="yukmenang">
                                <h4 className="font-weight-bold">Tertarik untuk jadi Pemenang?</h4>
                                <h4>Segera daftarkan dirimu sekarang!</h4>
                            </div>
                        </div>
                    </div>
                    <div className="mx-5 row d-flex align-items-center">
                        <div className="col-12">
                            <a href="/kompetisi/voice/registrasi" className="mx-2 btn btn-success">Daftar</a>
                            {/* <a href="/todo" className="mx-2 btn btn-outline-dark">Kontak Kami</a> */}
                        </div>
                    </div>
                </div>

                <div className="row px-5 py-5">
                    <div className="col-12">
                        <div className="row mb-3">
                            <div className="col-12">
                                <h4 className="font-weight-bold text-center">Lomba Lainnya</h4>
                            </div>
                        </div>
                        <div className="slide">
                            <div hidden>
                                Icons made by 
                                <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a>
                                from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
                            </div>
                            <div className="row align-items-center">
                                {lombaList.map((lombaItem) => {
                                    return (
                                        <Carousel
                                            foto = {lombaItem.foto}
                                            kategori = {lombaItem.kategori}
                                            abu = {false}
                                            link = {lombaItem.link}
                                        />
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}