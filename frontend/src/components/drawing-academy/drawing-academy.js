import React from 'react';
// import { Link } from "react-router-dom";
import "./drawing-academy.css";
import Navbar from '../navbar/navbar';
import logo_cerpen from "../../image/cerpen-vektor.png";
import gambar from "../../image/gambar-vektor.png";
import esai from "../../image/esai-vektor.png";
import foto from "../../image/foto-vektor.png";
import poster from "../../image/poster-vektor.png";
import puisi from "../../image/puisi-vektor.png";
import mic from "../../image/microphone-vektor.png";
// import hadroh from "../../image/hadroh.png";
// import hadiah_cerpen from "../../image/cerpen-hadiah.png";
import Carousel from "../kompetisi/lomba-carousel"; 

export default function drawingAcademy() {

    const lombaList = [
        // {
        //     foto: gambar,
        //     kategori: 'Drawing Academy',
        //     link:"drawing-academy"
        // },
        {
            foto: foto,
            kategori: 'Photo Contest',
            link:"photo-contest"
        },
        {
            foto: poster,
            kategori: 'Creative Design',
            link:"creative-design"
        },
        {
            foto: puisi,
            kategori: 'In Words',
            link:"in-words"
        },
        {
            foto: logo_cerpen,
            kategori: 'In Story',
            link:"in-story"
        },
        {
            foto: esai,
            kategori: 'Writing Academy',
            link:"writing-academy"
        },
        {
            foto: mic,
            kategori: 'Voice',
            link:"voice"
        },
      ];

    return (
        <div>
            <Navbar
                kompetisi = "active"
            />
            <div className="container-fluid">
                <div className="row justify-content-center" id="cont-banner">
                    <div className="col-12" id="banner">

                        <div className="row mt-5 d-flex justify-content-center">
                            <div>
                                <img alt="logo" src={gambar} className="img-fluid float-right"></img>
                            </div>
                            <div className="mx-5 my-3">
                                <div className="row text-center">
                                    <div className="col-12">
                                        <h6 className="text-white">Kompetisi Online UIIBF 2020</h6>
                                        <h1 className="text-white">UI IBF Drawing Academy</h1>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row justify-content-center">
                            <div className="col-8">
                                <p className="text-center text-white" id="deskripsi-banner">
                                    UI IBF Drawing Academy adalah lomba menggambar 
                                yang diperuntukkan bagi siswa/i SD/sederajat 
                                kelas 3 hingga kelas 6. Lomba ini diselenggarakan 
                                untuk memberikan kesempatan kepada adik-adik 
                                Sekolah Dasar untuk dapat ikut serta mensyiarkan 
                                nilai-nilai keislaman melalui gambar.
                                </p>
                            </div>
                        </div>
                        <div className="row justify-content-center mt-3 mb-5">
                            <div className="col-4 text-center">
                                <a href="/kompetisi/drawing-academy/registrasi" className="btn btn-block btn-success text-white">Daftar</a>
                            </div>
                            {/* <div className="col-6"> */}
                            {/* </div> */}
                        </div>
                        {/* <div className="row justify-content-center mt-3 mb-5">
                            <div className="col-4 text-center">
                                <a href="/todo" className="btn btn-block btn-warning">Kontak Kami</a>
                            </div>
                        </div> */}
                    </div>
                </div>

                <div className="row mt-5 mb-2 justify-content-center" id="cont-timeline">
                    <div className="col-10">
                        <h3>Timeline</h3>
                        <ul>
                            <li>Pendaftaran : 15 November 2020 – 28 November 2020</li>
                            <li>Technical Meeting : Melalui Grup WhatsApp Peserta</li>
                            <liv>Waktu Pelaksanaan : 19 Desember 2020 (08.00 - 11.00 WIB)</liv>
                            <li>Batas pengiriman karya : 19 Desember 2020 (11.15 WIB)</li>
                            <li>Pengumuman : 27 Desember 2020 (saat acara puncak 6th UIIBF)</li>
                        </ul>
                    </div>
                </div>

                <div className="row my-2 justify-content-center text-justify" id="cont-syaratketentuan">
                    <div className="col-10">
                        <h3>Tema</h3>
                        <ul>
                            <li>“Melihat Dunia Melalui Buku”</li>
                        </ul>
                    </div>
                </div>

                <div className="row my-2 justify-content-center text-justify" id="cont-syaratketentuan">
                    <div className="col-10">
                        <h3>Syarat & Ketentuan</h3>
                        <ol>
                            <li>Gambar yang dibuat harus memperhatikan nilai-nilai Islam</li>
                            <li>Peserta diwajibkan menggambar logo 6th UI IBF</li>
                            <li>Proses menggambar secara langsung melalui Zoom Cloud Meeting</li>
                            <li>Gambar dikirimkan dalam bentuk file (.pdf) atau foto (.jpg/.jpeg) dengan format judul file “Gambar_(nama peserta)_(judul gambar)”</li>
                            <li>Gambar dikirimkan ke email: kompetisiuiibf@gmail.com dengan format judul email “Pengiriman karya_Gambar_(nama peserta)_(judul gambar)”</li>
                        </ol>
                    </div>
                </div>

                <div className="row mt-2 mb-5 justify-content-center text-justify" id="cont-kriteria">
                    <div className="col-10">
                        <h3>Kriteria Penilaian</h3>
                        <ol>
                            <li>Kerapihan dan kebersihan (10%)</li>
                            <li>Teknik menggambar dan mewarnai (30%)</li>
                            <li>Kreativitas ide gambar (30%)</li>
                            <li>Komposisi warna (20%)</li>
                            <li>Estetika (10%)</li>
                        </ol>
                    </div>
                </div>

                <div className="row py-5 d-flex justify-content-center bg-light">
                    <div className="mx-3">
                        <div className="row align-items-center">
                            <div className="col-12">
                                <h3 className="font-weight-bold text-center">Hadiah Kompetisi</h3>
                                <div className="row d-flex justify-content-center">
                                    <div className="ml-3 mr-3">
                                        <p>Pemenang akan mendapatkan</p>
                                        <ul>
                                            <li>Uang Pembinaan</li>
                                            <li>E-Sertifikat</li>
                                        </ul>
                                    </div>
                                    <div className="">
                                        <p>Peserta akan mendapatkan</p>
                                        <ul>
                                            <li>E-Sertifikat peserta</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <div className="mx-5">
                        <img alt="hadiah cerpen" src={hadiah_cerpen} className="img-fluid float-right"></img>
                    </div> */}
                </div>

                <div className="row py-5 d-flex justify-content-center">
                    <div className="mx-3">
                        <div className="row">
                            <div className="col-12" id="yukmenang">
                                <h4 className="font-weight-bold">Tertarik untuk jadi Pemenang?</h4>
                                <h4>Segera daftarkan dirimu sekarang!</h4>
                            </div>
                        </div>
                    </div>
                    <div className="mx-5 row d-flex align-items-center">
                        <div className="col-12">
                            <a href="/kompetisi/drawing-academy/registrasi" className="mx-2 btn btn-success">Daftar</a>
                            {/* <a href="/todo" className="mx-2 btn btn-outline-dark">Kontak Kami</a> */}
                        </div>
                    </div>
                </div>

                <div className="row px-5 py-5">
                    <div className="col-12">
                        <div className="row mb-3">
                            <div className="col-12">
                                <h4 className="font-weight-bold text-center">Lomba Lainnya</h4>
                            </div>
                        </div>
                        <div className="slide">
                            <div hidden>
                                Icons made by 
                                <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a>
                                from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
                            </div>
                            <div className="row align-items-center">
                                {lombaList.map((lombaItem) => {
                                    return (
                                        <Carousel
                                            foto = {lombaItem.foto}
                                            kategori = {lombaItem.kategori}
                                            abu = {false}
                                            link = {lombaItem.link}
                                        />
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}