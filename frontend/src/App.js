import React from "react";
import './App.css';
import { BrowserRouter, Route, Switch } from "react-router-dom";

import Footer from "./components/footer/footer";
import Kompetisi from "./components/kompetisi/kompetisi";
import Homepage from "./components/homepage/homepage";
import InStory from "./components/in-story/in-story";
import DrawAcad from "./components/drawing-academy/drawing-academy";
import PhotoCont from "./components/photo-contest/photo-contest";
import CreativeDes from "./components/creative-design/creative-design";
import InWords from "./components/in-words/in-words";
import WritingAcad from "./components/writing-academy/writing-academy";
import Voice from "./components/voice/voice";
import Registrasi from "./components/registrasi/registrasi";
import Acara from "./components/acara/acara";
import SelfDev from "./components/acara/selfDevelopment";
import Workshop from "./components/acara/workshop";
import BedahBuku from "./components/acara/bedahBuku";
import Inovasi from "./components/acara/inovasiTeknologi";
import WorkshopLagi from "./components/acara/workshopLagi";

function App() {
  
  return (
      <BrowserRouter>
        <div>
          <Switch>
            {/* Sub Kompetisi Sub Registrasi */}
            <Route path="/kompetisi/drawing-academy/registrasi" render={(props) => (<Registrasi {...props} lomba="Drawing Academy" link="drawing-academy" kategori="Lomba"/>)}></Route>
            <Route path="/kompetisi/photo-contest/registrasi" render={(props) => (<Registrasi {...props} lomba="Photo Contest" link="photo-contest" kategori="Lomba"/>)}></Route>
            <Route path="/kompetisi/creative-design/registrasi" render={(props) => (<Registrasi {...props} lomba="Creative Design" link="creative-design" kategori="Lomba"/>)}></Route>
            <Route path="/kompetisi/in-words/registrasi" render={(props) => (<Registrasi {...props} lomba="In Words" link="in-words" kategori="Lomba"/>)}></Route>
            <Route path="/kompetisi/in-story/registrasi" render={(props) => (<Registrasi {...props} lomba="In Story" link="in-story" kategori="Lomba"/>)}></Route>
            <Route path="/kompetisi/writing-academy/registrasi" render={(props) => (<Registrasi {...props} lomba="Writing Academy" link="writing-academy" kategori="Lomba"/>)}></Route>
            <Route path="/kompetisi/voice/registrasi" render={(props) => (<Registrasi {...props} lomba="Voice" link="voice" kategori="Lomba"/>)}></Route>

            {/* Sub Kompetisi */}
            <Route path="/kompetisi/drawing-academy" component={DrawAcad}></Route>
            <Route path="/kompetisi/photo-contest" component={PhotoCont}></Route>
            <Route path="/kompetisi/creative-design" component={CreativeDes}></Route>
            <Route path="/kompetisi/in-words" component={InWords}></Route>
            <Route path="/kompetisi/in-story" component={InStory}></Route>
            <Route path="/kompetisi/writing-academy" component={WritingAcad}></Route>
            <Route path="/kompetisi/voice" component={Voice}></Route>

            {/* Sub Acara */}
            <Route path="/acara/self-development" component={SelfDev}></Route>
            <Route path="/acara/workshop-1" component={Workshop}></Route>
            <Route path="/acara/workshop-2" component={WorkshopLagi}></Route>
            <Route path="/acara/bedah-buku" component={BedahBuku}></Route>
            <Route path="/acara/inovasi-teknologi" component={Inovasi}></Route>

            {/* Sub Regis Acara */}
            {/* <Route path="/acara/daftar/self-development" render={(props) => (<Registrasi {...props} lomba="Self Development" link="self-development" kategori="Acara"/>)}></Route> */}
            {/* <Route path="/acara/daftar/workshop-1" render={(props) => (<Registrasi {...props} lomba="Workshop-1" link="workshop-1" kategori="Acara"/>)}></Route> */}
            <Route path="/acara/daftar/workshop-2" render={(props) => (<Registrasi {...props} lomba="Workshop-2" link="workshop-2" kategori="Acara"/>)}></Route>
            {/* <Route path="/acara/daftar/bedah-buku" render={(props) => (<Registrasi {...props} lomba="Bedah Buku" link="bedah-buku" kategori="Acara"/>)}></Route> */}
            <Route path="/acara/daftar/inovasi-teknologi" render={(props) => (<Registrasi {...props} lomba="Inovasi Teknologi" link="inovasi-teknologi" kategori="Acara"/>)}></Route>
            
            <Route path="/kompetisi" component={Kompetisi}></Route>
            <Route path="/acara" component={Acara}></Route>
            <Route exact path="/" component={Homepage}></Route>
          </Switch>
          <Footer />
        </div>
      </BrowserRouter>
  );
}

export default App;
