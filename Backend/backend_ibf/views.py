from django.views.decorators.csrf import ensure_csrf_cookie
from django.shortcuts import render
from django.middleware.csrf import get_token
from django.http import JsonResponse

def csrf(request):
    return JsonResponse({'csrfToken': get_token(request)})