from django.core.exceptions import ObjectDoesNotExist
from django.contrib.admin.views.decorators import staff_member_required
from django.http.response import JsonResponse, Http404, HttpResponse
from django.shortcuts import render,redirect
from io import BytesIO as IO
from .models import Registrasi
from .serializers import RegistrasiSerializer
from rest_framework.parsers import JSONParser 
from rest_framework.decorators import api_view
from rest_framework import status
from .support_function import send_email,generate_harga_unik,send_mail_awal, cek_user
from user.views import user_list_create
import datetime
import json
import threading
import pandas as pd

@api_view(['POST'])
def registrasi_list_create(request):
    if request.method == 'POST':
        payload = JSONParser().parse(request)
        data_user = user_list_create(payload["user"])
        if data_user is None:
            kode_unik = 1
            for obj in Registrasi.objects.all():
                if int(obj.id_regis.split()[1]) > kode_unik:
                    kode_unik = int(obj.id_regis.split()[1])
            kode_unik += 1
            if (int(payload["nominal_bayar"])!=0):
                temp = generate_harga_unik(payload["nominal_bayar"]) 
                payload.update({"status" : "Menunggu Pembayaran"})
            else:
                temp = "0"
                payload.update({"status" : "Pembayaran Dikonfirmasi"})
            payload.update({"id_regis" : "REG " + str(kode_unik)})
            payload.update({"nominal_bayar" : int(temp)})
            if (payload["tipe"] == "Lomba"):
                payload.update({"tipe" : "Kompetisi"})
            regis_serializer = RegistrasiSerializer(data=payload)
            if regis_serializer.is_valid():
                regis_serializer.save()
                thread = threading.Thread(target=send_mail_awal, args=(payload,))
                thread.start()
                return JsonResponse({"registrasi": payload["nominal_bayar"]}, status=status.HTTP_201_CREATED, safe=False)
            print(regis_serializer.errors)
            return JsonResponse({"error": regis_serializer.errors}, status=status.HTTP_400_BAD_REQUEST)
        return JsonResponse({"error": data_user}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def cek_pembayaran(request):
    if request.method == 'POST':
        signature = request.headers["Signature"]
        payload = JSONParser().parse(request)
        
        try:
            data_regis = Registrasi.objects.get(nominal_bayar=payload[0]["amount"])
            data_regis.status = "Pembayaran Dikonfirmasi"
            data_regis.tanggal_pembayaran = datetime.datetime.now()
            data_regis.save()
            send_email(data_regis)
        except ObjectDoesNotExist:
            return JsonResponse({"error": "Registrasi Not Found!"}, status=status.HTTP_400_BAD_REQUEST)
        return JsonResponse({"Status":"Update berhasil"}, safe=False, status=status.HTTP_200_OK)

@staff_member_required
def registrasi_list(request):
    if request.method == "GET":
        regis = Registrasi.objects.all()
        regis_serializer = RegistrasiSerializer(regis, many=True)
        regis_header = (
            'Id Regis',
            'Email',
            'Nama Lengkap',
            'Tipe Acara',
            'Nama Acara',
            'Nominal Bayar',
            'Status',
            'Update Status',
            'Delete Data'
        )
        for data in regis_serializer.data:
            data["id_split"] = data["id_regis"].split()[0] + "_" + data["id_regis"].split()[1]
        return render(request,'list_data.html',context={"registrasi": regis_serializer.data,"regis_header":regis_header})
    raise Http404('Page tidak ditemukan!')

@staff_member_required
def registrasi_update(request, id_regis):
    split_id = id_regis.split("_")
    real_id = split_id[0] + " " + split_id[1]
    if request.method == "GET":
        regis = Registrasi.objects.filter(id_regis=real_id)
        if regis:
            send_email(regis)
            regis[0].status = "Pembayaran Dikonfirmasi"
            regis[0].save()
            return redirect(registrasi_list)
        raise Http404('Tidak ada regis dengan id tersebut')
    raise Http404('Page tidak ditemukan!')

@staff_member_required
def registrasi_delete(request, id_regis):
    split_id = id_regis.split("_")
    real_id = split_id[0] + " " + split_id[1]
    if request.method == "GET":
        regis = Registrasi.objects.filter(id_regis=real_id)
        if regis:
            regis.delete()
            return redirect(registrasi_list)
        raise Http404('Tidak ada regis dengan id tersebut')
    raise Http404('Page tidak ditemukan!')

@staff_member_required
def export_xls(request):
    if request.method == "GET":
        regis = Registrasi.objects.all()
        regis_serializer = RegistrasiSerializer(regis, many=True)
        dictionary = {}
        i = 0
        for data in regis_serializer.data:
            for key,user_data in data["registrasi_user"].items():
                data[key] = user_data
            del data["registrasi_user"]
            dictionary[i] = data
            i += 1
        df = pd.DataFrame.from_dict(dictionary, orient='index')
        excel_file = IO()

        tipe_acara = [
            'Drawing Academy',
            'Photo Contest',
            'Creative Design',
            'In Words',
            'In Story',
            'Writing Academy',
            'Voice',
            'Workshop-1',
            'Workshop-2',
            'Bedah Buku',
            'Inovasi Teknologi',
            'Self Development'
        ]

        xlwriter = pd.ExcelWriter(excel_file, engine='xlsxwriter')
        for tipe in tipe_acara:
            df_filter = df.loc[df['nama'] == tipe]
            df_filter.to_excel(xlwriter, tipe)
        xlwriter.save()
        excel_file.seek(0)
        response = HttpResponse(excel_file.read(), content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        response['Content-Disposition'] = 'attachment; filename=data_regis.xlsx'
        return response


