from django.db import models
from user.models import User

tipe_choices = [
    ("Acara","Acara"),
    ("Kompetisi","Kompetisi")
]

status_choices = [
    ("Menunggu Pembayaran","Menunggu Pembayaran"),
    ("Pembayaran Dikonfirmasi","Pembayaran Dikonfirmasi")
]

# Create your models here.
class Registrasi(models.Model):
    id_regis = models.CharField(primary_key=True, max_length=20)
    nominal_bayar = models.IntegerField()
    tanggal_regis = models.DateField(auto_now_add=True)
    status = models.CharField(max_length=40, choices=status_choices, default="Menunggu Pembayaran")
    registrasi_user = models.ForeignKey(to=User, on_delete=models.CASCADE)
    nama = models.TextField()
    tipe = models.CharField(max_length=10, choices=tipe_choices)
    tanggal_pembayaran = models.DateTimeField(blank=True,null=True)

    class Meta:
        ordering = ['id_regis']
