from crontab import CronTab
from django.core.management.base import BaseCommand, CommandError
from django.core.mail import send_mail
from registrasi.models import Registrasi
from registrasi.support_function import cek_user

class Command(BaseCommand):
    help = 'Scheduler untuk cek konfirmasi'

    def handle(self, *args, **options):
        regis_all = Registrasi.objects.all()
        for regis in regis_all:
            self.check_time(regis)
        with CronTab(user="mufalando") as cron:
            cron.remove_all()
    
    def check_time(self, obj):
        recipient = obj.registrasi_user.email
        if obj.status == "Menunggu Pembayaran":
            if obj.tipe == "Acara":
                send_mail(
                    subject= "Konfirmasi Registrasi Acara UI IBF 2020",
                    message = "Acara gagal",
                    from_email = "muhfathurh@gmail.com",
                    recipient_list = [recipient,],
                    fail_silently = False
                )
                cek_user(recipient)
            else:
                send_mail(
                    subject= "Konfirmasi Registrasi Kompetisi UI IBF 2020",
                    message = "Kompetisi gagal",
                    from_email = "muhfathurh@gmail.com",
                    recipient_list = [recipient,],
                    fail_silently = False
                )
            obj.delete()
            cek_user(recipient)

