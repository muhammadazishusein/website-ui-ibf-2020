from django.core.management.base import BaseCommand, CommandError
from django.core.mail import send_mail
from django.template.loader import render_to_string
from registrasi.models import Registrasi
from registrasi.support_function import generate_harga_unik
import datetime

class Command(BaseCommand):
    help = 'Fixing Data'

    def handle(self, *args, **options):
        regis_all_1 = Registrasi.objects.filter(nama="Workshop-1")
        regis_all_2 = Registrasi.objects.filter(nama="Workshop-2")
        self.change_harga_regis(regis_all_1)
        self.change_harga_regis(regis_all_2)
    
    def change_harga_regis(self,regis_list):
        for regis in regis_list:
            if (regis.nominal_bayar >= 1000):
                regis.nominal_bayar = generate_harga_unik("0")
                regis.save()
                self.send_mail_ralat(regis)
        
    def change_harga_regis(self,regis_list):
        for regis in regis_list:
            if (regis.nominal_bayar >= 16000 or regis.nominal_bayar <= 14999):
                if (regis.nama != "Workshop-1" and regis.nama != "Workshop-2"):
                    regis.nominal_bayar = generate_harga_unik("15000")
                    regis.save()
                    self.send_mail_ralat(regis)
    
    def send_mail_ralat(self,regis):
        now = datetime.datetime.now()
        context = {
            'nama_lengkap': regis.registrasi_user.nama_lengkap,
            'nominal_bayar': regis.nominal_bayar,
            'email': regis.registrasi_user.email,
            'nama': regis.nama,
            'tanggal_regis': now.strftime("%H:%M:%S, %d %b %Y"),
        }
        msg_html = render_to_string('email_reminder_acara.html', context)

        send_mail(
            subject= "[Ralat Pembayaran Infaq] 6th UIIBF",
            message = "",
            from_email = "uiibf.2020@gmail.com",
            recipient_list = [context["email"],],
            fail_silently = False,
            html_message=msg_html
        )