from django.core.management.base import BaseCommand, CommandError
from django.core.mail import send_mail
from django.template.loader import render_to_string
from registrasi.models import Registrasi
from registrasi.support_function import generate_harga_unik
from registrasi.serializers import RegistrasiSerializer
from user.views import user_list_create
import csv
import datetime
import re

class Command(BaseCommand):
    def handle(self, *args, **options):
        regis_all_3 = Registrasi.objects.filter(nama="Bedah Buku").values_list('registrasi_user__email',flat=True)
        with open('static/bedah_buku.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            csv_reader.__next__()
            for regis in csv_reader:
                if regis[1] not in regis_all_3:
                    if re.match("0",regis[3]):
                        new_phone = re.sub(r"^0", "+62", regis[3])
                    elif re.match("8",regis[3]):
                        new_phone = re.sub(r"^8", "+62", regis[3])
                    else:
                        new_phone = regis[3]
                    temp = {
                        "email": regis[1],
                        "domisili": regis[8],
                        "institusi_pendidikan": regis[4],
                        "phone_number": new_phone,
                        "nama_lengkap": regis[2]
                    }
                    user = user_list_create(temp)
                    kode_unik = 1
                    for obj in Registrasi.objects.all():
                        if int(obj.id_regis.split()[1]) > kode_unik:
                            kode_unik = int(obj.id_regis.split()[1])
                    kode_unik += 1
                    payload = {
                        "nama" : "Bedah Buku",
                        "tipe" : "Acara",
                        "status": "Pembayaran Dikonfirmasi",
                        "id_regis": "REG " + str(kode_unik),
                        "nominal_bayar": "0",
                        "registrasi_user": regis[1],
                        "user": temp
                    }
                    regis_serializer = RegistrasiSerializer(data=payload)
                    if regis_serializer.is_valid():
                        regis_serializer.save()
        regis_all_3 = Registrasi.objects.filter(nama="Bedah Buku")
        for regis in regis_all_3:
            self.send_mail_buku(regis)
                        
    def send_mail_buku(self, data):
        now = datetime.datetime.now()
        context = {
            'nama_lengkap': data.registrasi_user.nama_lengkap,
            'nominal_bayar': data.nominal_bayar,
            'email': data.registrasi_user.email,
            'tanggal_regis': now.strftime("%H:%M:%S, %d %b %Y"),
            'nama': data.nama,
        }
        msg_html = render_to_string('email_link_bedah_buku.html', context)

        send_mail(
            subject= "[Ralat Konfirmasi Pendaftaran] Bedah Buku 6th UIIBF",
            message = "",
            from_email = "uiibf.2020@gmail.com",
            recipient_list = [context["email"],],
            fail_silently = False,
            html_message=msg_html
        )

