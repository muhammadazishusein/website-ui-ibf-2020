from crontab import CronTab
from django.core.management.base import BaseCommand, CommandError
import datetime

class Command(BaseCommand):
    help = 'Memulai Cron Task'

    def handle(self, *args, **options):
        with CronTab(user="rofi") as cron:
            job = cron.new(command="source /home/rofi/website-ui-ibf-2020/django-venv/bin/activate &&  python3 /home/rofi/website-ui-ibf-2020/Backend/manage.py check_expiry")
            job.setall(datetime.datetime(2020, 12, 15, 23, 59, 59, 90000))