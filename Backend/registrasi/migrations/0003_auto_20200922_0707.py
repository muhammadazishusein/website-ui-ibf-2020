# Generated by Django 3.0.9 on 2020-09-22 00:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('registrasi', '0002_auto_20200915_0940'),
    ]

    operations = [
        migrations.AddField(
            model_name='registrasi',
            name='nomor_unik',
            field=models.CharField(max_length=3, unique=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='registrasi',
            name='status',
            field=models.CharField(choices=[('Menunggu Pembayaran', 'Menunggu Pembayaran'), ('Pembayaran Dikonfirmasi', 'Pembayaran Dikonfirmasi'), ('Waktu Pembayaran Sudah Habis', 'Waktu Pembayaran Sudah Habis'), ('Menunggu Konfirmasi SS IG', 'Menunggu Konfirmasi SS IG'), ('Format Gambar Salah', 'Format Gambar Salah')], default='Menunggu Pembayaran', max_length=40),
        ),
    ]
