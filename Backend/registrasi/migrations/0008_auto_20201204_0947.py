# Generated by Django 3.1 on 2020-12-04 02:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('registrasi', '0007_auto_20201129_1417'),
    ]

    operations = [
        migrations.AlterField(
            model_name='registrasi',
            name='nominal_bayar',
            field=models.IntegerField(unique=True),
        ),
        migrations.AlterField(
            model_name='registrasi',
            name='nomor_unik',
            field=models.CharField(max_length=3),
        ),
    ]
