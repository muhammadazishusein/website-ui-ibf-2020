from rest_framework import serializers 
from .models import Registrasi
from user.serializers import UserSerializer

# Still need to be checked, converting JSON to date maybe lead to more serializer class
class RegistrasiSerializer(serializers.ModelSerializer):

    class Meta:
        model = Registrasi
        fields = (
            'id_regis',
            'nominal_bayar',
            'tanggal_regis',
            'registrasi_user',
            'tipe',
            'nama',
            'status',
            'tanggal_pembayaran'
        )
    
    def to_representation(self, instance):
        self.fields['registrasi_user'] =  UserSerializer(read_only=True)
        return super(RegistrasiSerializer, self).to_representation(instance)