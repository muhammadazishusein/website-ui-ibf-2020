import hmac, hashlib

SHARED_SECRET = 'vUIh1RRW'

def verifySignature(string_to_verify, signature):
	return ct_compare(hmac.new(SHARED_SECRET, 
		string_to_verify, hashlib.sha512).digest(), signature)

def ct_compare(a, b):
	"""
	** From Django source **
	Run a constant time comparison against two strings
	Returns true if a and b are equal.
	a and b must both be the same length, or False is 
	returned immediately
	"""
	if len(a) != len(b):
		return False

	result = 0
	for ch_a, ch_b in zip(a, b):
		result |= ord(ch_a) ^ ord(ch_b)
	return result == 0
