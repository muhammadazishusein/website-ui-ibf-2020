from django.urls import path
from .views import registrasi_list_create, cek_pembayaran,registrasi_list, registrasi_update, export_xls, registrasi_delete

urlpatterns = [ 
    path('registrasi/', registrasi_list_create),
    path('registrasi/cek_pembayaran/', cek_pembayaran),
    path('registrasi/list_data/', registrasi_list),
    path('registrasi/list_data/<str:id_regis>/', registrasi_update),
    path('registrasi/list_data/delete/<str:id_regis>/', registrasi_update),
    path('registrasi/export/', export_xls)
]