from crontab import CronTab
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils import timezone
from .models import Registrasi
from user.views import delete_user
import datetime
import random

def send_email(regis):
    
    context = {
        'nama_lengkap': regis.registrasi_user.nama_lengkap,
        'nominal_bayar': regis.nominal_bayar,
        'email': regis.registrasi_user.email,
        'nama': regis.nama,
        'phone_number': regis.registrasi_user.phone_number
    }

    if regis.nama == "Workshop-1":
        msg_html = render_to_string('email_konfirmasi_workshop_1.html', context)
        subject_msg = "[Konfirmasi Pembayaran Infaq ] Link Grup Workshop 6th UIIBF"
    elif regis.nama == "Workshop-2":
        msg_html = render_to_string('email_konfirmasi_workshop_2.html', context)
        subject_msg = "[ Konfirmasi Infaq UIIBF ] Workshop CV dan Wawancara"
    elif regis.tipe == "Acara":
        msg_html = render_to_string('email_konfirmasi_acara.html', context)
        subject_msg = "[ Konfirmasi Infaq UIIBF ] Seminar Inovasi dan Teknologi"
    else:
        msg_html = render_to_string('email_konfirmasi_kompetisi.html', context)
        subject_msg = "[Konfirmasi Pembayaran Infaq] 6th UIIBF"
        
    send_mail(
            subject= subject_msg,
            message = "",
            from_email = "uiibf.2020@gmail.com",
            recipient_list = [context["email"],],
            fail_silently = False,
            html_message=msg_html
        )


def generate_harga_unik(nominal):
    list_nomor_unik = set(Registrasi.objects.filter(status="Menunggu Pembayaran").values_list("nominal_bayar",flat=True))
    semua_angka = set(range(int(nominal),int(nominal)+1000))
    list_nomor_baru = semua_angka - list_nomor_unik
    nomor_baru = random.choice(list(list_nomor_baru))
    return nomor_baru

def cek_user(email):
    if not(Registrasi.objects.filter(registrasi_user=email).exists()):
        delete_user(email)

def send_mail_awal(payload):
        now = datetime.datetime.now()
        context = {
            'nama_lengkap': payload["user"]["nama_lengkap"],
            'nominal_bayar': payload["nominal_bayar"],
            'email': payload["user"]["email"],
            'tanggal_regis': now.strftime("%H:%M:%S, %d %b %Y"),
            'nama': payload["nama"],
            'phone_number': payload["user"]["phone_number"]
        }
        
        if payload["nama"] == "Bedah Buku":
            msg_html = render_to_string('email_link_bedah_buku.html', context)
            subject_msg = "Pendaftaran Bedah Buku UIIBF"
        elif payload["nama"] == "Workshop-1":
            if payload["nominal_bayar"] == 0:
                msg_html = render_to_string('email_konfirmasi_workshop_1.html', context)
                subject_msg = "[Konfirmasi Pembayaran Infaq ] Link Grup Workshop 6th UIIBF"
            else:
                msg_html = render_to_string('email_pembayaran_workshop_1.html', context)
                subject_msg = "[Reminder Pembayaran Infaq ] Workshop 6th UIIBF"
        elif payload["nama"] == "Workshop-2":
            if payload["nominal_bayar"] == 0:
                msg_html = render_to_string('email_konfirmasi_workshop_2.html', context)
                subject_msg = "[ Konfirmasi Infaq UIIBF ] Workshop CV dan Wawancara"
            else:
                msg_html = render_to_string('email_pembayaran_workshop_2.html', context)
                subject_msg = "[ Reminder Infaq UIIBF ] Workshop CV dan Wawancara"
        elif payload["tipe"] == "Acara":
            if payload["nominal_bayar"] == 0:
                msg_html = render_to_string('email_konfirmasi_acara.html', context)
                subject_msg = "[ Konfirmasi Infaq UIIBF ] Seminar Inovasi dan Teknologi"
            else:
                msg_html = render_to_string('email_reminder_acara.html', context)
                subject_msg = " [ Reminder Infaq UIIBF ] Seminar Inovasi dan Teknologi"
        elif payload["tipe"].lower() == "kompetisi":
            subject_msg = "Reminder untuk membayar infaq UIIBF"
            msg_html = render_to_string('email_regist_kompetisi.html', context)
        send_mail(
            subject= "Reminder untuk membayar infaq UIIBF",
            message = "",
            from_email = "uiibf.2020@gmail.com",
            recipient_list = [context["email"],],
            fail_silently = False,
            html_message=msg_html
        )
