from django.shortcuts import render
from .models import User
from .serializers import UserSerializer
from rest_framework import viewsets
from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework.decorators import api_view
from rest_framework import status
from django.core.exceptions import ObjectDoesNotExist

def user_list_create(data_user):
    try:
        cek_user = User.objects.get(email=data_user['email'])
    except ObjectDoesNotExist:
        pass
    else:
        return None
    users_serializer = UserSerializer(data=data_user)
    if users_serializer.is_valid():
        users_serializer.save()
        return None
    return users_serializer.errors

def delete_user(user):
    try:
        cek_user = User.objects.get(email=user)
        cek_user.delete()
    except ObjectDoesNotExist:
        pass
    

    

