from rest_framework import serializers 
from .models import User
 
class UserSerializer(serializers.ModelSerializer):
  class Meta:
    model = User
    fields = ('email',
              'domisili',
              'institusi_pendidikan',
              'phone_number',
              'nama_lengkap'
            )