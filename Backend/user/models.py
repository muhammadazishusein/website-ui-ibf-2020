from django.db import models
from django.core.validators import RegexValidator

# Create your models here.
class User(models.Model):
    email = models.EmailField(primary_key=True)
    domisili = models.TextField()
    institusi_pendidikan = models.CharField(max_length=50)
    phone_regex = RegexValidator(regex=r'^\+62?\d{9,15}', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone_number = models.CharField(validators=[phone_regex], max_length=14) # validators should be a list
    nama_lengkap = models.CharField(max_length=100)
